package ir.asta.sample.dl.ui.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class PageObject {

	protected WebDriver driver;
	protected Actions actions;

	public PageObject(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		actions = new Actions(driver);
		webDriverWait(waitCondition());
	}

	protected abstract ExpectedCondition<WebElement> waitCondition();

	protected void sendKeys(WebElement webElement, CharSequence keysToSend, boolean clear) {
		webDriverWait(ExpectedConditions.elementToBeClickable(webElement));
		if (clear)
			webElement.clear();
		webElement.sendKeys(keysToSend);
	}

	protected void webDriverWait(ExpectedCondition<WebElement> condition) {
		WebDriverWait wdw = new WebDriverWait(driver, 15);
		wdw.until(condition);
	}

	protected void threadWait(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
		}
	}

	protected void threadWait() {
		threadWait(2);
	}

	protected void click(WebElement webElement) {
		try {
			actions.moveToElement(webElement).click().perform();
		} catch (WebDriverException e) {
			webElement.click();
		}
	}
}