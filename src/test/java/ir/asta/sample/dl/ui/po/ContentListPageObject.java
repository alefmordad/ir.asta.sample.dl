package ir.asta.sample.dl.ui.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Predicate;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ContentListPageObject extends PageObject {

	@FindBy(id = "btn-create")
	private WebElement createButton;

	@FindBy(id = "btn-clear-search")
	private WebElement clearSearchInputsButton;

	@FindBy(id = "search_content_code")
	private WebElement codeSearchInput;

	@FindBy(id = "btn-search")
	private WebElement searchButton;

	private String firstRowXPath = "//*[@id=\"routeContainer\"]/div/div/div[2]/div/table/tbody/tr[1]";

	public String getFirstRowCode() {
		WebElement firstRowCode = driver.findElement(By.xpath(firstRowXPath + "/td[5]"));
		return firstRowCode.getAttribute("innerHTML").trim();
	}

	public ContentEditPageObject openFirstRowEdit() {
		WebElement firstRowEditButton = driver.findElement(By.xpath(firstRowXPath + "/td[12]/button[1]"));
		click(firstRowEditButton);
		return new ContentEditPageObject(driver);
	}

	public void search() {
		String currentUrl = driver.getCurrentUrl();
		click(searchButton);
		WebDriverWait wdw = new WebDriverWait(driver, 10);
		Predicate<WebDriver> urlHasChanged = driver -> !driver.getCurrentUrl().equals(currentUrl);
		wdw.until(urlHasChanged);
	}

	public ContentEditPageObject openCreateContent() {
		click(createButton);
		return new ContentEditPageObject(driver);
	}

	public void setCodeSearchParam(String code) {
		sendKeys(codeSearchInput, code, true);
	}

	public void clearSearchParams() {
		click(clearSearchInputsButton);
	}

	public ContentListPageObject(WebDriver driver) {
		super(driver);
	}

	@Override
	protected ExpectedCondition<WebElement> waitCondition() {
		return ExpectedConditions.elementToBeClickable(createButton);
	}

}
