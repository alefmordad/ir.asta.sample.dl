package ir.asta.sample.dl.ui;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import ir.asta.sample.dl.ui.po.ContentEditPageObject;
import ir.asta.sample.dl.ui.po.ContentListPageObject;
import ir.asta.sample.dl.ui.po.LoginPageObject;
import ir.asta.sample.dl.ui.po.PortalIndexPageObject;
import ir.asta.sample.dl.util.converters.ResourceNameToPathConverter;

public class ContentEntityTest extends FunctionalTest {

	@Test
	public void createTest() throws InterruptedException {
		driver.get("https://localhost:3456/portal");

		LoginPageObject loginPO = new LoginPageObject(driver);
		PortalIndexPageObject portalIndexPO = loginPO.login("root", "root1234");

		ContentListPageObject contentListPO = portalIndexPO.openContentList();
		
		ContentEditPageObject contentEditPO = contentListPO.openCreateContent();
		
		contentEditPO.setFilePath(new ResourceNameToPathConverter().convert("test-context.xml"));
		contentEditPO.setPurchaseTime(LocalDate.of(1396, 10, 9));
		contentEditPO.setCode("CONTENT_MATRIX");
		contentEditPO.setSubject("ماتریکس");
		contentEditPO.setContentType("فيلم");
		contentEditPO.setLibrary("مخبر");
		contentEditPO.setConfidentialLevel("محرمانه");
		contentListPO = contentEditPO.clickSaveOrUpdate();

		contentListPO.clearSearchParams();
		contentListPO.setCodeSearchParam("CONTENT_MATRIX");
		contentListPO.search();
		assertEquals("CONTENT_MATRIX", contentListPO.getFirstRowCode());

		contentEditPO = contentListPO.openFirstRowEdit();
		assertEquals("CONTENT_MATRIX", contentEditPO.getCode());
	}

}
