package ir.asta.sample.dl.ui.po;

import java.time.LocalDate;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import ir.asta.sample.dl.util.converters.LocalDateToKeyConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ContentEditPageObject extends PageObject {

	@FindBy(id = "edit_content_file")
	private WebElement fileInput;

	@FindBy(id = "edit_content_purchaseTime")
	private WebElement purchaseTimeInput;

	@FindBy(id = "edit_content_code")
	private WebElement codeInput;

	@FindBy(id = "edit_content_subject")
	private WebElement subjectInput;

	@FindBy(id = "edit_content_contentType")
	private WebElement contentTypeInput;

	@FindBy(id = "edit_content_library")
	private WebElement libraryInput;

	@FindBy(id = "edit_content_confidentialLevel")
	private WebElement confidentialLevelInput;

	@FindBy(id = "btn-edit-saveOrUpdate")
	private WebElement saveButton;

	@FindBy(id = "btn-edit-back")
	private WebElement backButton;

	public String getCode() {
		return codeInput.getAttribute("value").trim();
	}

	public void setFilePath(String fileName) {
		sendKeys(fileInput, fileName, true);
	}

	public void setPurchaseTime(LocalDate date) {
		sendKeys(purchaseTimeInput, new LocalDateToKeyConverter().convert(date), true);
	}

	public ContentListPageObject clickSaveOrUpdate() {
		click(saveButton);
		try {
			click(backButton);
		} catch (NoSuchElementException e) {
		}
		return new ContentListPageObject(driver);
	}

	public void setContentType(String contentType) {
		sendKeys(contentTypeInput, contentType, false);
	}

	public void setLibrary(String library) {
		sendKeys(libraryInput, library, false);
	}

	public void setCode(String code) {
		sendKeys(codeInput, code, true);
	}

	public void setSubject(String subject) {
		sendKeys(subjectInput, subject, true);
	}

	public void setConfidentialLevel(String confidentialLevel) {
		sendKeys(confidentialLevelInput, confidentialLevel, false);
	}

	public ContentEditPageObject(WebDriver driver) {
		super(driver);
	}

	@Override
	protected ExpectedCondition<WebElement> waitCondition() {
		return ExpectedConditions.elementToBeClickable(fileInput);
	}

}
