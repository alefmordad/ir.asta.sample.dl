package ir.asta.sample.dl.ui.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PortalIndexPageObject extends PageObject {

	@FindBy(xpath = "//*[@id=\"pageHeaderRoot\"]/div/div")
	private WebElement menu;

	public ContentListPageObject openContentList() {
		WebElement dlRootMenu = menu.findElement(By.xpath("//*[@id=\"pageHeaderRoot\"]/div/div/ul/li[2]/a/span[1]"));
		click(dlRootMenu);
		WebElement dlFormsMenu = dlRootMenu
				.findElement(By.xpath("//*[@id=\"pageHeaderRoot\"]/div/div/ul/li[2]/ul/li[1]/a"));
		click(dlFormsMenu);
		WebElement dlContentListMenu = dlFormsMenu
				.findElement(By.xpath("//*[@id=\"pageHeaderRoot\"]/div/div/ul/li[2]/ul/li[1]/ul/li[2]/a"));
		click(dlContentListMenu);
		return new ContentListPageObject(driver);
	}
	
	@Override
	public ExpectedCondition<WebElement> waitCondition() {
		return ExpectedConditions.elementToBeClickable(menu);
	}

	public PortalIndexPageObject(WebDriver driver) {
		super(driver);
	}

}
