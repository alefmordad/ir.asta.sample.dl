package ir.asta.sample.dl.ui.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class LoginPageObject extends PageObject {

	@FindBy(id = "username")
	private WebElement username;

	@FindBy(id = "password")
	private WebElement password;

	@FindBy(id = "loginButton")
	private WebElement loginButton;

	@Override
	public ExpectedCondition<WebElement> waitCondition() {
		return ExpectedConditions.elementToBeClickable(username);
	}

	public PortalIndexPageObject login(String username, String password) {
		sendKeys(this.username, username, true);
		sendKeys(this.password, password, true);
		click(loginButton);
		return new PortalIndexPageObject(driver);
	}

	public LoginPageObject(WebDriver driver) {
		super(driver);
	}

}
