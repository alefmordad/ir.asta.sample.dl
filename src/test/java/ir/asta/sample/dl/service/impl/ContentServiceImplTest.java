package ir.asta.sample.dl.service.impl;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;

import ir.asta.sample.dl.entities.ConfidentialLevelEntity;
import ir.asta.sample.dl.entities.ContentEntity;
import ir.asta.sample.dl.manager.ConfidentialLevelManager;
import ir.asta.sample.dl.manager.ContentManager;
import ir.asta.wise.core.security.SecurityContextUtils;
import ir.asta.wise.core.security.model.Post;
import ir.asta.wise.core.security.model.User;
import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class ContentServiceImplTest {

	@Tested
	private ContentServiceImpl contentService;
	@Injectable
	private ContentManager contentManager;
	@Injectable
	private ConfidentialLevelManager confidentialLevelManager;
	@Mocked
	private SecurityContextUtils securityContextUtils;

	private ConfidentialLevelEntity confidentialLevel = new ConfidentialLevelEntity();
	private Post post = new Post();
	private User user = new User();

	@Test
	public void confidentialLevelAuthorization_ContentWithHigherConfidentialLevelThanUser_Exception() {
		confidentialLevel.setLevelIndex(2L);
		post.setConfidentialLevel(1L);
		user.setCurrentPost(post);
		new NonStrictExpectations() {
			{
				SecurityContextUtils.getCurrentUser();
				result = user;
			}
		};
		ContentEntity contentEntity = new ContentEntity();
		contentEntity.setConfidentialLevel(confidentialLevel);
		try {
			contentService.confidentialLevelAuthorization(contentEntity);
			fail();
		} catch (Exception e) {
		}
	}

	@Test
	public void confidentialLevelAuthorization_ContentWithLowerConfidentialLevelThanUser_NoException() {
		confidentialLevel.setLevelIndex(1L);
		post.setConfidentialLevel(2L);
		user.setCurrentPost(post);
		new NonStrictExpectations() {
			{
				SecurityContextUtils.getCurrentUser();
				result = user;
			}
		};
		ContentEntity contentEntity = new ContentEntity();
		contentEntity.setConfidentialLevel(confidentialLevel);
		try {
			contentService.confidentialLevelAuthorization(contentEntity);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void confidentialLevelAuthorization_ContentWithSameConfidentialLevelAsUser_NoException() {
		confidentialLevel.setLevelIndex(2L);
		post.setConfidentialLevel(2L);
		user.setCurrentPost(post);
		new NonStrictExpectations() {
			{
				SecurityContextUtils.getCurrentUser();
				result = user;
			}
		};
		ContentEntity contentEntity = new ContentEntity();
		contentEntity.setConfidentialLevel(confidentialLevel);
		try {
			contentService.confidentialLevelAuthorization(contentEntity);
		} catch (Exception e) {
			fail();
		}
	}

}