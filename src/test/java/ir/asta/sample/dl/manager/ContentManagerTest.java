package ir.asta.sample.dl.manager;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import ir.asta.sample.dl.dao.ContentDao;
import ir.asta.sample.dl.entities.ContentEntity;
import ir.asta.sample.dl.entities.ContentStatus;
import ir.asta.wise.core.util.db.SequenceGenerator;
import ir.asta.wise.portal.security.service.query.user.UserQueryFactory;
import mockit.Injectable;
import mockit.NonStrictExpectations;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class ContentManagerTest {

	@Tested
	private ContentManager contentManager;
	@Injectable
	private UserQueryFactory userQueryFactory;
	@Injectable
	private SequenceGenerator sequenceGenerator;
	@Injectable
	private ContentDao contentDao;

	private ContentEntity oldContent = new ContentEntity();
	private ContentEntity newContent = new ContentEntity();

	{
		oldContent.setId("1");
		newContent.setId("1");
	}

	@Test
	public void beforeMerge_AcceptedToAccepted_NoExcepion() {
		oldContent.setContentStatus(ContentStatus.ACCEPTED);
		newContent.setContentStatus(ContentStatus.ACCEPTED);

		new NonStrictExpectations() {
			{
				contentManager.load(anyString);
				result = oldContent;
			}
		};

		try {
			contentManager.beforeMerge(newContent, false);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void beforeMerge_RejectedToRejected_NoExcepion() {
		oldContent.setContentStatus(ContentStatus.REJECTED);
		newContent.setContentStatus(ContentStatus.REJECTED);

		new NonStrictExpectations() {
			{
				contentManager.load(anyString);
				result = oldContent;
			}
		};

		try {
			contentManager.beforeMerge(newContent, false);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void beforeMerge_AcceptedToWaiting_Excepion() {
		oldContent.setContentStatus(ContentStatus.ACCEPTED);
		newContent.setContentStatus(ContentStatus.WAITING);

		new NonStrictExpectations() {
			{
				contentManager.load(anyString);
				result = oldContent;
			}
		};

		try {
			contentManager.beforeMerge(newContent, false);
			fail();
		} catch (Exception e) {
		}
	}

	@Test
	public void beforeMerge_AcceptedToRejected_Excepion() {
		oldContent.setContentStatus(ContentStatus.ACCEPTED);
		newContent.setContentStatus(ContentStatus.REJECTED);

		new NonStrictExpectations() {
			{
				contentManager.load(anyString);
				result = oldContent;
			}
		};

		try {
			contentManager.beforeMerge(newContent, false);
			fail();
		} catch (Exception e) {
		}
	}

	@Test
	public void beforeMerge_RejectedToAccepted_Excepion() {
		oldContent.setContentStatus(ContentStatus.REJECTED);
		newContent.setContentStatus(ContentStatus.ACCEPTED);

		new NonStrictExpectations() {
			{
				contentManager.load(anyString);
				result = oldContent;
			}
		};

		try {
			contentManager.beforeMerge(newContent, false);
			fail();
		} catch (Exception e) {
		}
	}

	@Test
	public void beforeMerge_RejectedToWaiting_Excepion() {
		oldContent.setContentStatus(ContentStatus.REJECTED);
		newContent.setContentStatus(ContentStatus.WAITING);

		new NonStrictExpectations() {
			{
				contentManager.load(anyString);
				result = oldContent;
			}
		};

		try {
			contentManager.beforeMerge(newContent, false);
			fail();
		} catch (Exception e) {
		}
	}

}
