package ir.asta.sample.dl.manager;

import ir.asta.sample.dl.dao.SystemSettingDao;
import ir.asta.sample.dl.entities.SystemSettingEntity;
import mockit.Mock;
import mockit.MockUp;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class SystemSettingManagerTest {

	@BeforeClass
	public static void init() {

		new MockUp<SystemSettingDao>() {

			@Mock
			public SystemSettingEntity load(String id) {
				return null;
			}

		};

	}

	@Test
	public void testSomeMethod() {

	}

}
