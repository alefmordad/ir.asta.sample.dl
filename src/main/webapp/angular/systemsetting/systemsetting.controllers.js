angular.module('dl.systemsetting')
    .controller('SystemsettingEditCtrl', ['flash', '$scope', 'SystemsettingResource', 'dlControllerService', function (flash, $scope, SystemsettingResource, dlControllerService) {
        var loading = $.fn.showLoading();
    	dlControllerService.init($scope);
     	dlControllerService.applyEditCtrl($scope, 'systemSetting', SystemsettingResource, function (data) {
     		$scope.data = data;
     		$.fn.hideLoading(loading);
        });

     	$scope.mySave = function() {
     		var loading = $.fn.showLoading();
     		SystemsettingResource.update($scope.data).$promise.then(function(data) {
          	    $.fn.hideLoading(loading);
     			if (data.success) {
     				flash.success = data.message;
     			} else {
     	            flash.error = data.message;
     	        }
     		}, function(error) {
     			$.fn.hideLoading(loading);
                flash.error = error.data.message;
     		});
     	};
     	
    }])

    .controller('SystemsettingListCtrl', ['$scope', 'SystemsettingResource', 'dlControllerService', function ($scope, SystemsettingResource, dlControllerService) {
        var loading = $.fn.showLoading();
    	dlControllerService.init($scope);
     	dlControllerService.applyListCtrl($scope,'systemSetting', SystemsettingResource, function (data) {
     		$.fn.hideLoading(loading);
        });
    }])
    
    .controller('SystemsettingDisplayCtrl', ['$scope', '$location', '$routeParams', 'SystemsettingResource',  'dlControllerService', function ($scope, $location, $routeParams, SystemsettingResource,  dlControllerService) {
        var loading = $.fn.showLoading();
        dlControllerService.init($scope);
        dlControllerService.applyDisplayCtrl($scope,'systemSetting', SystemsettingResource, function (data) {
          $.fn.hideLoading(loading);
        });
      
      
      
    }]);
