angular.module('dl.systemsetting',['ngRoute',
   ['/dl/angular/lib.js'
    ,'/dl/angular/systemsetting/systemsetting.controllers.js'
    ,'/dl/angular/systemsetting/systemsetting.services.js'
    ,'/dl/css/ng-tags-input.min.css'
    ,'/dl/css/ng-tags-input.bootstrap.min.css'
    ,'/dl/js/ng-tags-input.min.js']])
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/dl/systemsetting/list', {templateUrl: '/dl/angular/systemsetting/systemsetting.list.html', controller: 'SystemsettingListCtrl'})
      .when('/dl/systemsetting/edit', {templateUrl: '/dl/angular/systemsetting/systemsetting.edit.html', controller: 'SystemsettingEditCtrl'})
      .when('/dl/systemsetting/edit/:id', {templateUrl: '/dl/angular/systemsetting/systemsetting.edit.html', controller: 'SystemsettingEditCtrl'})
      .when('/dl/systemsetting/display/:id', {templateUrl: '/dl/angular/systemsetting/systemsetting.display.html', controller: 'SystemsettingDisplayCtrl'})
      	
  }]);
