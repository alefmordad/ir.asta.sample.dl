angular.module('dl.systemsetting')
    .factory('SystemsettingResource', ['$resource', 'dlResourceService', function ($resource, dlResourceService) {
      return $resource('', {}, dlResourceService.create('systemSetting'));
    }]);
