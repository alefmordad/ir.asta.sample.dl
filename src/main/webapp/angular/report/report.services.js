angular.module('dl.report')
    .factory('ReportResource', ['$resource', 'dlResourceService', function ($resource, dlResourceService) {
    	var ReportResource = {contentByContentType: {method: 'GET', url:'/dl/rest/report/contentByContentType'}};
    	return $resource('', {}, ReportResource);
    }]);
