angular.module('dl.report')
    .controller('ReportContentByContentTypeCtrl', ['$modalInstance', '$scope', 'ReportResource', 'dlControllerService', 'data', function ($modalInstance, $scope, ReportResource, dlControllerService, data) {
    	dlControllerService.init($scope);
     	dlControllerService.applyMinimalCtrl($scope, 'report', ReportResource);
     	
     	$scope.searchParam = data.searchParam;     	
     	$scope.searchParam['pageSize'] = 1;
     	$scope.searchParam['pageNumber'] = -1; // means all of it
        var loading = $.fn.showLoading();
     	ReportResource.contentByContentType($scope.searchParam).$promise.then(function(data) {
     		$scope.data = data;
     		$.fn.hideLoading(loading);
     	});
     	
     	$scope.back = function() {
     		$modalInstance.dismiss();
     	}
   }])