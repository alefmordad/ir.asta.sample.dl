angular.module('dl.report',['ngRoute',
   ['/dl/angular/lib.js'
    ,'/dl/angular/report/report.controllers.js'
    ,'/dl/angular/report/report.services.js']])
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/dl/report/contentByContentType', {templateUrl: '/dl/angular/report/report.contentByContentType.html', controller: 'ReportContentByContentTypeCtrl'})
  }]);
