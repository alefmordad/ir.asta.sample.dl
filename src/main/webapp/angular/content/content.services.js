angular.module('dl.content')
    .factory('ContentResource', ['$resource', 'dlResourceService', function ($resource, dlResourceService) {
    	var ContentResource = dlResourceService.create('content');
    	ContentResource.enums = {method: 'GET', url: '/dl/rest/content/enums'};
    	ContentResource.updateContentStatus = {method: 'POST', url: '/dl/rest/content/updateContentStatus'};
    	return $resource('', {}, ContentResource);
    }]);
