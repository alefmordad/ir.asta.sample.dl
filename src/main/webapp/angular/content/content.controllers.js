angular.module('dl.content')
    .controller('ContentEditCtrl', ['$scope', 'ContentResource', 'dlControllerService', function ($scope, ContentResource, dlControllerService) {
      var loading = $.fn.showLoading();
    	dlControllerService.init($scope);
     	dlControllerService.applyEditCtrl($scope,'content', ContentResource, function (data) {
     		$.fn.hideLoading(loading);
     		$scope.data = data;
     		$scope.onContentTypeChanged();
     	});
    
        $scope.onContentTypeChanged = function() {
        	$scope.validLibraries = [];
        	if ($scope.data.contentType != null) {
		       	for (i = 0; i <$scope.data.options.library.items.length; i++) {
		       		if ($scope.data.options.library.items[i].contentType.pk == $scope.data.contentType.pk) {
		       			$scope.validLibraries.push($scope.data.options.library.items[i])
		       		}
		       	}
        	}
        };
        
    }])

    .controller('ContentListCtrl', ['dialogs', '$route', '$filter', '$scope', 'ContentResource', 'dlControllerService', function (dialogs, $route, $filter, $scope, ContentResource, dlControllerService) {
      var loading = $.fn.showLoading();
    	dlControllerService.init($scope);
     	dlControllerService.applyListCtrl($scope,'content', ContentResource, function (data) {
           $.fn.hideLoading(loading);
        });
     	
     	$scope.editContentStatus = function() {
            var selectedItems = $filter('filter')($scope.data.items, {'$selected': true, 'contentStatus': 'WAITING'});
     		dialogs.create("/dl/angular/content/contentStatus.edit.html", "ListContentStatusEditDialogCtrl", {items: selectedItems}, {}).result.then(function() {
				var loading = $.fn.showLoading();
				$route.reload();
				$.fn.hideLoading(loading);
			});
     	};
     	
     	$scope.hasStatusEdiableInSelectedItems = function() {
            var selectedItems = $filter('filter')($scope.data.items, {'$selected': true});
            for (var idx in selectedItems) {
                var item = selectedItems[idx];
                if (item.contentStatus == "WAITING") return true;
              }
            return false;
     	};
     	
     	$scope.displayReportByContentType = function() {
         	var searchParam = $scope.actions.list.getSearchParams($scope.filter);
     		dialogs.create("/dl/angular/report/report.contentByContentType.html", "ReportContentByContentTypeCtrl", {searchParam: searchParam}, {});
     	};
    }])
    
    .controller('ListContentStatusEditDialogCtrl', ['flash', 'data', '$modalInstance', '$scope', 'ContentResource', function(flash, data, $modalInstance, $scope, ContentResource) {
    	$scope.data = data;
    	ContentResource.enums().$promise.then(function(data) {
    		$.extend($scope.data, {enums: {contentStatus: data.contentStatus}});
    	});
    	
    	$scope.back = function() {
    		$modalInstance.dismiss();
    	};
    	
    	$scope.save = function() {
			var successCount = 0;
			var successMessage;
			var failMessage;
			var loading = $.fn.showLoading();
			for (var i = 0; i < $scope.data.items.length; i++) {
				item = $scope.data.items[i];
	    		ContentResource.updateContentStatus({pk: item.pk, contentStatus: $scope.data.contentStatus}).$promise.then(function(data) {
	    			if (data.success) {
	    				successCount++;
        				successMessage = data.message;
	    			} else {
	    				failMessage = data.message;
	    			}
	    			if (i >= $scope.data.items.length - 1) {
		    			if (successCount > 0) {
		    		        flash.success = successCount + " " + successMessage;
		    			} else {
		    		        flash.error = data.message;
		    			}
						$.fn.hideLoading(loading);
		    			$modalInstance.close();
		    		}
	    		});
			}	
	    	
    	};
    }])
    
    .controller('ContentHistoryCtrl', ['$scope', '$window', '$routeParams', 'ContentResource', '$filter', '$location', function($scope, $window, $routeParams, ContentResource, $filter, $location) {
      var searchVersion = {id:$routeParams.id};
    	ContentResource.searchVersions(searchVersion).$promise.then(function(data){
    		for(k in data.items){
    			data.items[k].revInfo.revisionDateStr = $filter('persianDate')(data.items[k].revInfo.revisionDate,'fullDate') + ' - ' +$filter('date')(data.items[k].revInfo.revisionDate,'H:mm');
    		}
    		$scope.items = data.items;
    	});
    	
    	$scope.back = function() {
    		$window.history.back();
    	};
    	
    	$scope.showVersion = function(pk, revisionNumber) {
    		$location.path('/dl/content/version/'+pk+'/'+revisionNumber);
    	};
      }])
      
     .controller('ContentVersionCtrl', ['$scope', '$window', '$routeParams', 'ContentResource', function($scope, $window, $routeParams, ContentResource) {
   		var pk = $routeParams.id;
    	var revisionNumber = $routeParams.revisionNumber;
    	ContentResource.loadVersion({id: pk, revision: revisionNumber}).$promise.then(function(data){
			$scope.data = data.entity;
		});
		
		$scope.back = function() {
    		$window.history.back();
    	};
		
    	}])
    	

    .controller('ContentDisplayCtrl', ['$route', '$scope', 'dialogs', '$location', '$routeParams', 'ContentResource',  'AttachmentResource', 'dlControllerService', function ($route, $scope, dialogs, $location, $routeParams, ContentResource,  AttachmentResource, dlControllerService) {
        var loading = $.fn.showLoading();
    	dlControllerService.init($scope);
        dlControllerService.applyDisplayCtrl($scope,'content', ContentResource, function (data) {
          $.fn.hideLoading(loading);
        });
      
      $scope.ShowHistory = function() {
    	$location.path('/dl/content/history/'+$routeParams.id); 
      };
      
      
		$scope.attachmentParams={
      hideSearchPanel: true,
      masterPK:$scope.id,
      'filter.eq:content.pk': $scope.id
    };
		
		$scope.editContentStatus = function() {
			ContentResource.enums().$promise.then(function(data) {
				dialogs.create("/dl/angular/content/contentStatus.edit.html", "DisplayContentStatusEditDialogCtrl", {enums: data, pk: $scope.data.pk}, {}).result.then(function() {
					var loading = $.fn.showLoading();
					$route.reload();
					$.fn.hideLoading(loading);
				});
			});
        };
		
    }])
    
    .controller('DisplayContentStatusEditDialogCtrl', ['$scope', 'flash', '$modalInstance', 'ContentResource', 'dlControllerService', 'data', function($scope, flash, $modalInstance, ContentResource, dlControllerService, data) {
    	$scope.data = data;
    	ContentResource.show({id: $scope.data.pk}).$promise.then(function(data) {
    		$scope.data.contentStatus = data.contentStatus;
    	});

    	$scope.back = function() {
    		$modalInstance.dismiss();
    	};
    	$scope.save = function() {
    		ContentResource.updateContentStatus({pk: $scope.data.pk, contentStatus: $scope.data.contentStatus}).$promise.then(function(data) {
    			if (data.success) {
                    flash.success = data.message;
    			} else {
                 flash.error = data.message;
               }
    			$modalInstance.close();
    		});
    	};
    }]);
    
    ;
