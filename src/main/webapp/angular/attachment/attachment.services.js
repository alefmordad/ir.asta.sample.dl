angular.module('dl.attachment')
    .factory('AttachmentResource', ['$resource', 'dlResourceService', function ($resource, dlResourceService) {
      return $resource('', {}, dlResourceService.create('attachment'));
    }]);
