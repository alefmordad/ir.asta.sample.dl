angular.module('dl.attachment')
    .controller('AttachmentEditCtrl', ['$scope', 'AttachmentResource', 'dlControllerService', function ($scope, AttachmentResource, dlControllerService) {
      var loading = $.fn.showLoading();
    	dlControllerService.init($scope);
    	
    	$scope.actions.edit.loadEntity = function (remoteResource, loadCallback) {
            var resource = $scope.id ? remoteResource.show({id: $scope.id, masterPK: $scope.parameters.masterPK}) : remoteResource.load({masterPK: $scope.parameters.masterPK});
            resource.$promise.then(function (data) {
              if (angular.isFunction(loadCallback)) {
                loadCallback(data);
              }
              $.extend(true, $scope, {data: $.fn.cleanse(data)});
            });
          };
          
     	dlControllerService.applyEditCtrl($scope,'attachment', AttachmentResource, function (data) {
     		$.fn.hideLoading(loading);
      });
    }])
     
 
    .controller('AttachmentListCtrl', ['$scope', 'AttachmentResource', 'dlControllerService', function ($scope, AttachmentResource, dlControllerService) {
        var loading = $.fn.showLoading();
    	dlControllerService.init($scope);
     	dlControllerService.applyListCtrl($scope,'attachment', AttachmentResource, function (data) {
        $.fn.hideLoading(loading);
      });
    }])

    .controller('AttachmentDisplayCtrl', ['$scope', '$location', '$routeParams', 'AttachmentResource',  'dlControllerService', function ($scope, $location, $routeParams, AttachmentResource,  dlControllerService) {
      var loading = $.fn.showLoading();
    	dlControllerService.init($scope);
      dlControllerService.applyDisplayCtrl($scope,'attachment', AttachmentResource, function (data) {
          $.fn.hideLoading(loading);
      });
      
      
      
    }]);
