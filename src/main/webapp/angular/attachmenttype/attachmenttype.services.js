angular.module('dl.attachmenttype')
    .factory('AttachmenttypeResource', ['$resource', 'dlResourceService', function ($resource, dlResourceService) {
      return $resource('', {}, dlResourceService.create('attachmentType'));
    }]);
