package ir.asta.sample.dl.service;
import ir.asta.wise.core.datamanagement.EntityRestService;
import javax.ws.rs.Path;

/*PROTECTED REGION ID(SystemSettingService Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.SystemSettingEntity;

@Path("/systemSetting/")
public interface SystemSettingService
		extends
			EntityRestService<SystemSettingEntity, java.lang.Long> {
	/*PROTECTED REGION ID(SystemSettingService Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
