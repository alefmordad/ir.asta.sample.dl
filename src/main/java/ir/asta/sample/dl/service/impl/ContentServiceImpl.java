package ir.asta.sample.dl.service.impl;

import static ir.asta.wise.core.Constants.OPTIONS_CODE;

import java.util.ArrayList;
import java.util.HashMap;
/*PROTECTED REGION ID(ContentServiceImpl Imports) ENABLED START*/
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import ir.asta.sample.dl.entities.ConfidentialLevelEntity;
import ir.asta.sample.dl.entities.ContentEntity;
import ir.asta.sample.dl.entities.ContentStatus;
import ir.asta.sample.dl.manager.ConfidentialLevelManager;
import ir.asta.sample.dl.manager.ContentManager;
import ir.asta.sample.dl.model.ContentStatusUpdateModel;
import ir.asta.sample.dl.service.ContentService;
import ir.asta.sample.dl.util.DlConstants;
import ir.asta.wise.core.crud.AbstractCrudService;
import ir.asta.wise.core.datamanagement.ActionResult;
import ir.asta.wise.core.datamanagement.DataPage;
import ir.asta.wise.core.datamanagement.EntityOptionItem;
import ir.asta.wise.core.datamanagement.SearchParam;
import ir.asta.wise.core.datamanagement.VersionEntity;
import ir.asta.wise.core.datamanagement.criteria.Filters;
import ir.asta.wise.core.exceptions.LocalizedException;
import ir.asta.wise.core.remoting.rs.Include;
import ir.asta.wise.core.security.AuthorizationException;
import ir.asta.wise.core.security.SecurityContextUtils;
import ir.asta.wise.core.security.annotations.Secure;
import ir.asta.wise.core.security.model.Post;
import ir.asta.wise.core.util.beancopier.Mapper;
import ir.asta.wise.core.util.beancopier.MapperEnabled;
import ir.asta.wise.core.util.locale.LocaleUtil;

@Named("contentService")
@MapperEnabled
public class ContentServiceImpl extends AbstractCrudService<ContentEntity, java.lang.String> implements ContentService {

	/* PROTECTED REGION ID(ContentServiceImpl Attributes) ENABLED START */

	@Inject
	private ConfidentialLevelManager confidentialLevelManager;

	/* PROTECTED REGION END */

	@Inject
	public ContentServiceImpl(ContentManager manager) {
		super.setManager(manager);
	}

	@Override
	public ActionResult<java.lang.String> saveOrUpdate(@Mapper(value = { "*", "confidentialLevel.pk",
			"confidentialLevel.toStr", "contentType.pk", "contentType.toStr", "file.*", "library.pk", "library.toStr",
			"adaptiveDataModel" }, exclude = { "adaptiveDataXML" }) ContentEntity entity) {
		return super.saveOrUpdate(entity);
	}

	@Override
	@Mapper(value = { "*", "items.*", "items.revInfo.*", "items.entity(ir.asta.sample.dl.entities.ContentEntity)",
			"items.entity.*" }, enrich = false)
	public DataPage<VersionEntity<ir.asta.sample.dl.entities.ContentEntity>> searchVersions(java.lang.String pk,
			HttpServletRequest request) {
		return super.searchVersions(pk, request);
	}

	@Override
	@Mapper(value = { "*", "revisionType.*", "revInfo.*", "entity(ir.asta.sample.dl.entities.ContentEntity)",
			"entity.*" }, enrich = false)
	public VersionEntity<ir.asta.sample.dl.entities.ContentEntity> loadVersion(java.lang.String pk, Long revId) {
		return super.loadVersion(pk, revId);
	}

	/* PROTECTED REGION ID(ContentServiceImpl Methods) ENABLED START */
	@Override
	@Mapper(value = "*", enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	@Include(url = "contentType/allOptions", responsePath = "options.contentType", enabled = "${param['options'] != 'false'}")
	@Include(url = "library/allExtraOptions", responsePath = "options.library", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/allConfidentialLevelOptions", responsePath = "options.confidentialLevel", enabled = "${param['options'] != 'false'}")
	public ContentEntity load() {
		return super.load();
	}

	@Override
	@Mapper(value = { "*", "confidentialLevel.pk", "confidentialLevel.toStr", "contentType.pk", "contentType.toStr",
			"file.*", "library.pk", "library.toStr", "adaptiveDataModel" }, enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/entityPermissions/${args[0]}", responsePath = "permissions")
	@Include(url = "contentType/allOptions", responsePath = "options.contentType", enabled = "${param['options'] != 'false'}")
	@Include(url = "library/allExtraOptions", responsePath = "options.library", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/allConfidentialLevelOptions", responsePath = "options.confidentialLevel", enabled = "${param['options'] != 'false'}")
	public ContentEntity load(java.lang.String id) {
		return super.load(id);
	}

	@Override
	protected ContentEntity doLoad(String id) {
		ContentEntity entity = super.doLoad(id);
		confidentialLevelAuthorization(entity);
		return entity;
	}

	@Override
	protected void doSave(ContentEntity entity) {
		if (SecurityContextUtils.isCurrentUserAuthorizedForPermission(DlConstants.PERMISSION_CONTENT_ACCEPT))
			entity.setContentStatus(ContentStatus.ACCEPTED);
		else
			entity.setContentStatus(ContentStatus.WAITING);
		confidentialLevelAuthorization(entity);
		super.doSave(entity);
	}

	@Override
	protected void doUpdate(ContentEntity entity) {
		confidentialLevelAuthorization(entity);
		super.doUpdate(entity);
	}

	@Override
	protected void doDelete(ContentEntity entity) {
		confidentialLevelAuthorization(entity);
		super.doDelete(entity);
	}

	@Override
	protected void doPurge(ContentEntity entity) {
		confidentialLevelAuthorization(entity);
		super.doPurge(entity);
	}

	@Override
	protected void doRestore(ContentEntity entity) {
		confidentialLevelAuthorization(entity);
		super.doRestore(entity);
	}

	@Override
	protected void doDeleteEntities(List<ContentEntity> items) {
		confidentialLevelAuthorization(items.toArray(new ContentEntity[items.size()]));
		super.doDeleteEntities(items);
	}

	@Override
	protected void doPurgeEntities(List<ContentEntity> items) {
		confidentialLevelAuthorization(items.toArray(new ContentEntity[items.size()]));
		super.doPurgeEntities(items);
	}

	@Override
	protected void doRestoreEntities(List<ContentEntity> items) {
		confidentialLevelAuthorization(items.toArray(new ContentEntity[items.size()]));
		super.doRestoreEntities(items);
	}

	public void confidentialLevelAuthorization(ContentEntity... entities) {
		for (ContentEntity entity : entities) {
			if (SecurityContextUtils.getCurrentUser().getCurrentPost() == null || SecurityContextUtils.getCurrentUser()
					.getCurrentPost().getConfidentialLevel() < entity.getConfidentialLevel().getLevelIndex())
				throw new AuthorizationException();
		}
	}

	@Override
	protected Map<String, Object> searchDefaultInternal() {
		Map<String, Object> filters = super.searchDefaultInternal();
		filters.put(Filters.EQUALS + DlConstants.PROP_CONTENT_STATUS, ContentStatus.ACCEPTED);
		return filters;
	}

	@Override
	@Mapper(value = { "*", "items.*", "items.attachments.toStr", "items.confidentialLevel.toStr",
			"items.contentType.toStr", "items.file.toStr", "items.library.toStr" }, enrich = false)
	public DataPage<ContentEntity> search(HttpServletRequest request) {
		if (SecurityContextUtils.getCurrentUser().getCurrentPost() == null) {
			DataPage<ContentEntity> dataPage = new DataPage<>();
			dataPage.setItems(new ArrayList<ContentEntity>());
			dataPage.setTotalSize(0);
			return dataPage;
		}
		return super.search(request);
	}

	@Override
	protected SearchParam<Map<String, Object>> createSearchParam(HttpServletRequest request) {
		SearchParam<Map<String, Object>> searchParam = super.createSearchParam(request);
		Post currentPost = SecurityContextUtils.getCurrentUser().getCurrentPost();
		Long confidentialLevel = currentPost.getConfidentialLevel();
		addToSearchFilter(searchParam, Filters.LESS_OR_EQUAL + DlConstants.PROP_CONFIDENTIAL_LEVEL + Filters.DOT
				+ DlConstants.PROP_LEVEL_INDEX, confidentialLevel);
		return searchParam;
	}

	@Override
	public Map<String, Boolean> getEntityPermissions(String pk) {
		Map<String, Boolean> permissions = getPermissionsInternal(pk);
		permissions.put(DlConstants.PERMISSION_CONTENT_ACCEPT_CAMELCASE,
				SecurityContextUtils.isCurrentUserAuthorizedForPermission(DlConstants.PERMISSION_CONTENT_ACCEPT));
		return permissions;
	}

	@Override
	public Map<String, Boolean> getPermissions() {
		Map<String, Boolean> permissions = getPermissionsInternal(null);
		permissions.put(DlConstants.PERMISSION_CONTENT_ACCEPT_CAMELCASE,
				SecurityContextUtils.isCurrentUserAuthorizedForPermission(DlConstants.PERMISSION_CONTENT_ACCEPT));
		return permissions;
	}

	@Override
	@Secure(value = DlConstants.PERMISSION_CONTENT_ACCEPT, onSuccess = DlConstants.MSG_CONTENT_UPDATE_CONTENTSTATUS_SUCCESS, onFailure = DlConstants.MSG_CONTENT_UPDATE_CONTENTSTATUS_FAILURE, localized = true)
	public ActionResult<String> updateContentStatus(@Mapper(value = { "*" }) ContentStatusUpdateModel contentModel) {
		return saveOrUpdate(contentModel.getEntity());
	}

	@Override
	public DataPage<EntityOptionItem<Long>> allConfidentialLevelOptions() {
		return allConfidentialLevelOptionsInternal();
	}

	public DataPage<EntityOptionItem<Long>> allConfidentialLevelOptionsInternal() {
		try {
			authorizeSearchOptions();
			SearchParam<Map<String, Object>> searchParam = new SearchParam<Map<String, Object>>();
			searchParam.setFilter(new HashMap<String, Object>());
			Post currentPost = SecurityContextUtils.getCurrentUser().getCurrentPost();
			Long userConfidentialLevelIndex = currentPost == null ? 0 : currentPost.getConfidentialLevel();
			searchParam.getFilter().put(Filters.LESS_OR_EQUAL + DlConstants.PROP_LEVEL_INDEX,
					userConfidentialLevelIndex);
			searchParam.setPageSize(SearchParam.UNLIMITED_PAGE_SIZE);
			DataPage<ConfidentialLevelEntity> searchResult = confidentialLevelManager.doSearch(searchParam);
			DataPage<EntityOptionItem<Long>> res = EntityOptionItem.convertDataPage(searchResult);
			if (logReadOperations(OPTIONS_CODE)) {
				securityLog(Boolean.TRUE, getViewPermissionCode(), LocaleUtil.getText(SEC_LOG_LIST_VIEW_SUCCESS_KEY),
						getSingleName());
			}
			return res;
		} catch (Exception e) {
			throw new LocalizedException(getMessages(null, e), e);
		}
	}

	@Override
	@Mapper(value = "*", enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	@Include(url = "contentType/allOptions", responsePath = "options.contentType", enabled = "${param['options'] != 'false'}")
	@Include(url = "library/allOptions", responsePath = "options.library", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/allConfidentialLevelOptions", responsePath = "options.confidentialLevel", enabled = "${param['options'] != 'false'}")
	public Map<String, Object> searchDefault() {
		return super.searchDefault();
	}
	/* PROTECTED REGION END */

}
