package ir.asta.sample.dl.service.impl;
import java.util.Map;

import ir.asta.wise.core.crud.*;
import ir.asta.wise.core.datamanagement.*;

import ir.asta.wise.core.remoting.rs.Include;
import ir.asta.wise.core.util.beancopier.Mapper;
import ir.asta.wise.core.util.beancopier.MapperEnabled;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import ir.asta.wise.core.exceptions.LocalizedException;
import ir.asta.wise.core.util.locale.LocaleUtil;
import static ir.asta.wise.core.Constants.*;
/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.LibraryEntity;
import ir.asta.sample.dl.service.LibraryService;
import ir.asta.sample.dl.manager.LibraryManager;
import ir.asta.sample.dl.model.LibraryOptionItem;


@Named("libraryService")
@MapperEnabled
public class LibraryServiceImpl
		extends
			AbstractCrudService<LibraryEntity, java.lang.Long>
		implements
			LibraryService {

	/*PROTECTED REGION ID(LibraryServiceImpl Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	@Inject
	public LibraryServiceImpl(LibraryManager manager) {
		super.setManager(manager);
	}

	@Override
	@Mapper(value = "*", enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	@Include(url = "contentType/allOptions", responsePath = "options.contentType", enabled = "${param['options'] != 'false'}")
	@Include(url = "content/allOptions", responsePath = "options.contents", enabled = "${param['options'] != 'false'}")
	public Map<String, Object> searchDefault() {
		return super.searchDefault();
	}

	@Override
	@Mapper(value = "*", enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	@Include(url = "contentType/allOptions", responsePath = "options.contentType", enabled = "${param['options'] != 'false'}")
	public LibraryEntity load() {
		return super.load();
	}

	@Override
	@Mapper(value = {"*", "contentType.pk", "contentType.toStr"}, enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/entityPermissions/${args[0]}", responsePath = "permissions")
	@Include(url = "contentType/allOptions", responsePath = "options.contentType", enabled = "${param['options'] != 'false'}")
	public LibraryEntity load(java.lang.Long id) {
		return super.load(id);
	}

	@Override
	public ActionResult<java.lang.Long> saveOrUpdate(@Mapper(value = {"*",
			"contentType.pk", "contentType.toStr"}) LibraryEntity entity) {
		return super.saveOrUpdate(entity);
	}

	@Override
	@Mapper(value = {"*", "items.*", "items.contentType.toStr",
			"items.contents.toStr"}, enrich = false)
	public DataPage<LibraryEntity> search(HttpServletRequest request) {
		return super.search(request);
	}

	/*PROTECTED REGION ID(LibraryServiceImpl Methods) ENABLED START*/

	@Override
	@Mapper(value = {"*", "items.*", "items.contentType.pk"}, enrich = false)
	public DataPage<LibraryOptionItem> allExtraOptions(
			HttpServletRequest request) {
		return allOptionsExtraInternal(request);
	}

	protected DataPage<LibraryOptionItem> allOptionsExtraInternal(
			HttpServletRequest request) {
		try {
			authorizeSearchOptions();
			SearchParam<Map<String, Object>> searchParam = createSearchParamForOptions(request);
			searchParam.setPageSize(SearchParam.UNLIMITED_PAGE_SIZE);
			DataPage<LibraryEntity> searchResult = doSearch(searchParam);
			DataPage<LibraryOptionItem> res = LibraryOptionItem
					.convertFromDataPage(searchResult);
			if (logReadOperations(OPTIONS_CODE)) {
				securityLog(Boolean.TRUE, getViewPermissionCode(), LocaleUtil
						.getText(SEC_LOG_LIST_VIEW_SUCCESS_KEY),
						getSingleName());
			}
			return res;
		} catch (Exception e) {
			throw new LocalizedException(getMessages(null, e), e);
		}
	}

	/*PROTECTED REGION END*/

}
