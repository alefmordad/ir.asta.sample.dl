package ir.asta.sample.dl.service;
import ir.asta.wise.core.datamanagement.EntityRestService;
import javax.ws.rs.Path;

/*PROTECTED REGION ID(LibraryService Imports) ENABLED START*/

import ir.asta.sample.dl.entities.LibraryEntity;
import ir.asta.sample.dl.model.LibraryOptionItem;
import ir.asta.wise.core.datamanagement.DataPage;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.servlet.http.*;

/*PROTECTED REGION END*/

@Path("/library/")
public interface LibraryService
		extends
			EntityRestService<LibraryEntity, java.lang.Long> {
	/*PROTECTED REGION ID(LibraryService Methods) ENABLED START*/
	@Path("/allExtraOptions")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public DataPage<LibraryOptionItem> allExtraOptions(
			@Context HttpServletRequest request);
	/*PROTECTED REGION END*/
}
