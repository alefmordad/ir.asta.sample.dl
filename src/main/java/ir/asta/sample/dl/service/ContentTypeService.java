package ir.asta.sample.dl.service;
import ir.asta.wise.core.datamanagement.EntityRestService;
import javax.ws.rs.Path;

/*PROTECTED REGION ID(ContentTypeService Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.ContentTypeEntity;

@Path("/contentType/")
public interface ContentTypeService
		extends
			EntityRestService<ContentTypeEntity, java.lang.Long> {
	/*PROTECTED REGION ID(ContentTypeService Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
