package ir.asta.sample.dl.service.impl;
import static ir.asta.wise.core.Constants.SEARCH_CODE;

import java.util.Map;

import ir.asta.wise.core.crud.*;
import ir.asta.wise.core.datamanagement.*;
import ir.asta.wise.core.datamanagement.criteria.Filters;
import ir.asta.wise.core.exceptions.LocalizedException;
import ir.asta.wise.core.remoting.rs.Include;
import ir.asta.wise.core.util.beancopier.Mapper;
import ir.asta.wise.core.util.beancopier.MapperEnabled;
import ir.asta.wise.core.util.locale.LocaleUtil;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/*PROTECTED REGION ID(AttachmentTypeServiceImpl Imports) ENABLED START*/
import ir.asta.sample.dl.entities.ContentEntity;
import ir.asta.sample.dl.manager.ContentManager;
/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.AttachmentTypeEntity;
import ir.asta.sample.dl.service.AttachmentTypeService;
import ir.asta.sample.dl.util.DlConstants;
import ir.asta.sample.dl.manager.AttachmentTypeManager;


@Named("attachmentTypeService")
@MapperEnabled
public class AttachmentTypeServiceImpl
		extends
			AbstractCrudService<AttachmentTypeEntity, java.lang.Long>
		implements
			AttachmentTypeService {

	/*PROTECTED REGION ID(AttachmentTypeServiceImpl Attributes) ENABLED START*/
	@Inject
	private ContentManager contentManager;
	/*PROTECTED REGION END*/

	@Inject
	public AttachmentTypeServiceImpl(AttachmentTypeManager manager) {
		super.setManager(manager);
	}

	@Override
	@Mapper(value = "*", enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	@Include(url = "contentType/allOptions", responsePath = "options.contentType", enabled = "${param['options'] != 'false'}")
	@Include(url = "attachment/allOptions", responsePath = "options.attachments", enabled = "${param['options'] != 'false'}")
	public Map<String, Object> searchDefault() {
		return super.searchDefault();
	}

	@Override
	@Mapper(value = "*", enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	@Include(url = "contentType/allOptions", responsePath = "options.contentType", enabled = "${param['options'] != 'false'}")
	public AttachmentTypeEntity load() {
		return super.load();
	}

	@Override
	@Mapper(value = {"*", "contentType.pk", "contentType.toStr"}, enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/entityPermissions/${args[0]}", responsePath = "permissions")
	@Include(url = "contentType/allOptions", responsePath = "options.contentType", enabled = "${param['options'] != 'false'}")
	public AttachmentTypeEntity load(java.lang.Long id) {
		return super.load(id);
	}

	@Override
	public ActionResult<java.lang.Long> saveOrUpdate(@Mapper(value = {"*",
			"contentType.pk", "contentType.toStr"}) AttachmentTypeEntity entity) {
		return super.saveOrUpdate(entity);
	}

	@Override
	@Mapper(value = {"*", "items.*", "items.attachments.toStr",
			"items.contentType.toStr"}, enrich = false)
	public DataPage<AttachmentTypeEntity> search(HttpServletRequest request) {
		return super.search(request);
	}

	/*PROTECTED REGION ID(AttachmentTypeServiceImpl Methods) ENABLED START*/

	@Override
	@Mapper(value = {"*", "items.*"}, enrich = false)
	public DataPage<AttachmentTypeEntity> searchByContentId(String contentId) {
		ContentEntity contentEntity = contentManager.load(contentId);
		try {
			authorize(SEARCH_CODE);
			SearchParam<Map<String, Object>> searchParam = new SearchParam<>();
			addToSearchFilter(searchParam, Filters.EQUALS + DlConstants.PROP_CONTENT_TYPE + Filters.DOT + DlConstants.PROP_ID, contentEntity.getContentType().getId());
			DataPage<AttachmentTypeEntity> result = doSearch(searchParam);
			if (logReadOperations(SEARCH_CODE)) {
				securityLog(Boolean.TRUE, getSearchPermissionCode(), LocaleUtil.getText(SEC_LOG_LIST_VIEW_SUCCESS_KEY), getSingleName());
			}
			return result;
		} catch (Exception e) {
			securityLog(Boolean.FALSE, getSearchPermissionCode(), LocaleUtil.getText(SEC_LOG_LIST_VIEW_FAILURE_KEY), getSingleName());
			throw new LocalizedException(getMessages(null, e), e);
		}
	}
	
	/*PROTECTED REGION END*/

}
