package ir.asta.sample.dl.service.impl;
import java.util.Map;

import ir.asta.wise.core.crud.*;
import ir.asta.wise.core.datamanagement.*;
import ir.asta.wise.core.remoting.rs.Include;
import ir.asta.wise.core.util.beancopier.Mapper;
import ir.asta.wise.core.util.beancopier.MapperEnabled;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/*PROTECTED REGION ID(AttachmentServiceImpl Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.AttachmentEntity;
import ir.asta.sample.dl.service.AttachmentService;
import ir.asta.sample.dl.manager.AttachmentManager;

@Named("attachmentService")
@MapperEnabled
public class AttachmentServiceImpl
		extends
			AbstractCrudService<AttachmentEntity, java.lang.Long>
		implements
			AttachmentService {

	/*PROTECTED REGION ID(AttachmentServiceImpl Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	@Inject
	public AttachmentServiceImpl(AttachmentManager manager) {
		super.setManager(manager);
	}

	@Override
	@Mapper(value = "*", enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	@Include(url = "attachmentType/allOptions", responsePath = "options.attachmentType", enabled = "${param['options'] != 'false'}")
	public Map<String, Object> searchDefault() {
		return super.searchDefault();
	}

	@Override
	public ActionResult<java.lang.Long> saveOrUpdate(
			@Mapper(value = {"*", "attachmentType.pk", "attachmentType.toStr",
					"content.pk", "file.*"}) AttachmentEntity entity) {
		return super.saveOrUpdate(entity);
	}

	@Override
	@Mapper(value = {"*", "items.*", "items.attachmentType.toStr",
			"items.content.toStr", "items.file.toStr"}, enrich = false)
	public DataPage<AttachmentEntity> search(HttpServletRequest request) {
		return super.search(request);
	}

	/*PROTECTED REGION ID(AttachmentServiceImpl Methods) ENABLED START*/
	@Override
	@Mapper(value = {"*", "attachmentType.pk", "attachmentType.toStr",
			"content.pk", "content.toStr", "file.*"}, enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/entityPermissions/${args[0]}", responsePath = "permissions")
	@Include(url = "attachmentType/searchByContentId/${param['masterPK']}", responsePath = "options.attachmentType", enabled = "${param['options'] != 'false'}")
	public AttachmentEntity load(java.lang.Long id) {
		return super.load(id);
	}

	@Override
	@Mapper(value = "*", enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	@Include(url = "attachmentType/searchByContentId/${param['masterPK']}", responsePath = "options.attachmentType", enabled = "${param['options'] != 'false'}")
	public AttachmentEntity load() {
		return super.load();
	}
	/*PROTECTED REGION END*/

}
