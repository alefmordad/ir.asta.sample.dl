package ir.asta.sample.dl.service;
import ir.asta.wise.core.datamanagement.EntityRestService;


/*PROTECTED REGION ID(AttachmentTypeService Imports) ENABLED START*/
import ir.asta.wise.core.datamanagement.DataPage;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.AttachmentTypeEntity;

@Path("/attachmentType/")
public interface AttachmentTypeService
		extends
			EntityRestService<AttachmentTypeEntity, java.lang.Long> {
	/*PROTECTED REGION ID(AttachmentTypeService Methods) ENABLED START*/
	@GET
	@Path("/searchByContentId/{contentId}")
	@Produces(MediaType.APPLICATION_JSON)
	DataPage<AttachmentTypeEntity> searchByContentId(@PathParam("contentId") String contentId);
	/*PROTECTED REGION END*/
}
