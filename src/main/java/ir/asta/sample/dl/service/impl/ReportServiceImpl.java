package ir.asta.sample.dl.service.impl;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import ir.asta.sample.dl.manager.ReportManager;
import ir.asta.sample.dl.model.Report;
import ir.asta.sample.dl.service.ReportService;
import ir.asta.sample.dl.util.DlConstants;
import ir.asta.sample.dl.util.converters.HttpServletRequestToSearchParamConverter;
import ir.asta.wise.core.datamanagement.DataPage;
import ir.asta.wise.core.datamanagement.SearchParam;
import ir.asta.wise.core.datamanagement.criteria.Filters;
import ir.asta.wise.core.security.SecurityContextUtils;
import ir.asta.wise.core.security.model.Post;
import ir.asta.wise.core.util.beancopier.Mapper;
import ir.asta.wise.core.util.beancopier.MapperEnabled;

@Named("reportService")
@MapperEnabled
public class ReportServiceImpl implements ReportService {

	@Inject
	private ReportManager manager;

	@Override
	@Mapper(value = { "*", "items.*" }, enrich = false)
	public DataPage<Report> getContentReportByContentType(HttpServletRequest request) {
		DataPage<Report> dataPage = new DataPage<>();
		SearchParam<Map<String, Object>> searchParam = new HttpServletRequestToSearchParamConverter().convert(request);
		Post currentPost = SecurityContextUtils.getCurrentUser().getCurrentPost();
		Long userConfidentialLevelIndex = currentPost == null ? 0 : currentPost.getConfidentialLevel();
		searchParam.getFilter().put(Filters.LESS_OR_EQUAL + DlConstants.PROP_CONFIDENTIAL_LEVEL + Filters.DOT
				+ DlConstants.PROP_LEVEL_INDEX, userConfidentialLevelIndex);
		List<Report> items = manager.getContentByContentTypeReport(searchParam);
		dataPage.setItems(items);
		dataPage.setPageNumber(1);
		dataPage.setTotalSize(items.size());
		dataPage.setPageSize(items.size());
		return dataPage;
	}

}
