package ir.asta.sample.dl.service;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
/*PROTECTED REGION ID(ContentService Imports) ENABLED START*/
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ir.asta.sample.dl.entities.ContentEntity;
import ir.asta.sample.dl.model.ContentStatusUpdateModel;
import ir.asta.wise.core.datamanagement.ActionResult;
/*PROTECTED REGION END*/
import ir.asta.wise.core.datamanagement.DataPage;
import ir.asta.wise.core.datamanagement.EntityOptionItem;
import ir.asta.wise.core.datamanagement.EntityRestService;

@Path("/content/")
public interface ContentService
		extends
			EntityRestService<ContentEntity, java.lang.String> {
	/*PROTECTED REGION ID(ContentService Methods) ENABLED START*/
	@POST
	@Path("/updateContentStatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	ActionResult<String> updateContentStatus(ContentStatusUpdateModel contentModel);
	
	@GET
	@Path("/allConfidentialLevelOptions")
	@Produces(MediaType.APPLICATION_JSON)
	DataPage<EntityOptionItem<Long>> allConfidentialLevelOptions();
	
	/*PROTECTED REGION END*/
}
