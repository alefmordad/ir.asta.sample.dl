package ir.asta.sample.dl.service.impl;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/*PROTECTED REGION ID(SystemSettingServiceImpl Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.SystemSettingEntity;
import ir.asta.sample.dl.manager.SystemSettingManager;
import ir.asta.sample.dl.service.SystemSettingService;
import ir.asta.wise.core.crud.AbstractCrudService;
import ir.asta.wise.core.datamanagement.ActionResult;
import ir.asta.wise.core.datamanagement.DataPage;
import ir.asta.wise.core.remoting.rs.Include;
import ir.asta.wise.core.util.beancopier.Mapper;
import ir.asta.wise.core.util.beancopier.MapperEnabled;

@Named("systemSettingService")
@MapperEnabled
public class SystemSettingServiceImpl extends AbstractCrudService<SystemSettingEntity, java.lang.Long>
		implements SystemSettingService {

	/* PROTECTED REGION ID(SystemSettingServiceImpl Attributes) ENABLED START */

	/* PROTECTED REGION END */

	@Inject
	public SystemSettingServiceImpl(SystemSettingManager manager) {
		super.setManager(manager);
	}

	@Override
	@Mapper(value = "*", enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	public Map<String, Object> searchDefault() {
		return super.searchDefault();
	}

	@Override
	@Mapper(value = { "*", "config.*", "config.validFileFormats.*" }, enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/permissions", responsePath = "permissions")
	public SystemSettingEntity load() {
		List<SystemSettingEntity> settings = getManager().loadAll();
		if(settings.isEmpty())
			return null;
		return settings.get(0);
	}

	@Override
	@Mapper(value = { "*", "config.*", "config.validFileFormats.*" }, enrich = false)
	@Include(url = "#/enums", responsePath = "enums", enabled = "${param['options'] != 'false'}")
	@Include(url = "#/entityPermissions/${args[0]}", responsePath = "permissions")
	public SystemSettingEntity load(java.lang.Long id) {
		return super.load(id);
	}

	@Override
	public ActionResult<java.lang.Long> saveOrUpdate(
			@Mapper(value = { "*", "config.*", "config.validFileFormats.*" }) SystemSettingEntity entity) {
		return super.saveOrUpdate(entity);
	}

	@Override
	@Mapper(value = { "*", "items.*", "items.config.*", "config.validFileFormats.*" }, enrich = false)
	public DataPage<SystemSettingEntity> search(HttpServletRequest request) {
		return super.search(request);
	}

	/* PROTECTED REGION ID(SystemSettingServiceImpl Methods) ENABLED START */

	/* PROTECTED REGION END */

}
