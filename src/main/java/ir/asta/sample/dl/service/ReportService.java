package ir.asta.sample.dl.service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ir.asta.sample.dl.model.Report;
import ir.asta.wise.core.datamanagement.DataPage;

@Path("/report")
public interface ReportService {

	@GET
	@Path("/contentByContentType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	DataPage<Report> getContentReportByContentType(@Context HttpServletRequest request);

}
