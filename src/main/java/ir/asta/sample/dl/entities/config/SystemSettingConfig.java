package ir.asta.sample.dl.entities.config;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("config")
public class SystemSettingConfig {

	@XStreamAsAttribute
	private Long maxFileSize;

	@XStreamAsAttribute
	private List<String> validFileFormats;

	public Long getMaxFileSize() {
		return maxFileSize;
	}

	public void setMaxFileSize(Long maxFileSize) {
		this.maxFileSize = maxFileSize;
	}

	public List<String> getValidFileFormats() {
		return validFileFormats;
	}

	public void setValidFileFormats(List<String> validFileFormats) {
		this.validFileFormats = validFileFormats;
	}

}
