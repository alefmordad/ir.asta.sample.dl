package ir.asta.sample.dl.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import ir.asta.sample.dl.entities.config.SystemSettingConfig;
import ir.asta.wise.core.data.jpa.XmlAttributeConverter;
import ir.asta.wise.core.datamanagement.AbstractBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "SYSTEM_SETTING")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", doNotUseGetters = true, callSuper = false)
public class SystemSettingEntity extends AbstractBaseEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SYSTEM_SETTING_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Basic
	@Column(name = "CONFIG_XML", length = 5000)
	@Convert(converter = SystemSettingConfigConverter.class)
	private SystemSettingConfig config;

	@Override
	@Transient
	public Long getID() {
		return this.id;
	}

	@Override
	public void setID(Long id) {
		this.id = id;
	}

	public static class SystemSettingConfigConverter extends XmlAttributeConverter<SystemSettingConfig> {
		@Override
		protected Class<?>[] annotatedClasses() {
			return new Class[] { SystemSettingConfig.class };
		}
	}

}
