package ir.asta.sample.dl.entities;

import ir.asta.wise.core.util.locale.LocaleUtil;

public enum ContentStatus {
	ACCEPTED, REJECTED, WAITING;
	private static String PROP_BUNDLE_KEY = "contentStatus_";
	public String toString() {
		return LocaleUtil.getText(PROP_BUNDLE_KEY + this.name());
	}
}
