package ir.asta.sample.dl.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import ir.asta.wise.core.data.annotation.EditorType;
import ir.asta.wise.core.data.annotation.PropertyGenDirective;
import ir.asta.wise.core.data.annotation.SearchMethod;
import ir.asta.wise.core.datamanagement.AbstractBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/*PROTECTED REGION ID(ConfidentialLevelEntity : Imports) ENABLED START*/

/*PROTECTED REGION END*/

@Entity
@Table(name = "ORG_CONFIDENTIAL_LEVEL")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", doNotUseGetters = true, callSuper = false)
public class ConfidentialLevelEntity extends AbstractBaseEntity<java.lang.Long>
		implements
			Comparable<ConfidentialLevelEntity> {
	private static final long serialVersionUID = 1L;

	/*PROTECTED REGION ID(ConfidentialLevelEntity Attributes) ENABLED START*/
	
	/*PROTECTED REGION END*/

	// primary key
	@Id
	@Column(name = "CONFIDENTIAL_LEVEL_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private java.lang.Long id;

	// fields
	@Basic
	@Column(name = "TITLE", nullable = false)
	@PropertyGenDirective(searchMethod = SearchMethod.LIKE, editorType = EditorType.TEXT)
	private java.lang.String subject;

	@Basic
	@Column(name = "LEVEL_INDEX", nullable = false)
	@Min(1)
	@Max(1000)
	@PropertyGenDirective(searchMethod = SearchMethod.EQUAL, editorType = EditorType.AUTO)
	private java.lang.Long levelIndex;

	public int compareTo(ir.asta.sample.dl.entities.ConfidentialLevelEntity obj) {
		if (obj.hashCode() > hashCode())
			return 1;
		else if (obj.hashCode() < hashCode())
			return -1;
		else
			return 0;
	}
	@Override
	public String toString() {
		return toString(getSubject());
	}
	@Override
	@Transient
	protected String getToStringTemplate() {
		return "%s";
	}
	/**
	 * Don't remove this method
	 * This is related to some Lombok bugs!
	 */
	public java.lang.Long getId() {
		return id;
	}

	/**
	 * Don't remove this method
	 * This is related to some Lombok bugs!
	 */
	public void setId(java.lang.Long id) {
		this.id = id;
	}
	@Transient
	public java.lang.Long getID() {
		return this.id;
	}
	public void setID(java.lang.Long id) {
		this.id = id;
	}

	/*PROTECTED REGION ID(ConfidentialLevelEntity Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
