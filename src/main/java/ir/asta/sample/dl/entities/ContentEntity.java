package ir.asta.sample.dl.entities;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.gfaces.facelet.DataModel;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ir.asta.wise.core.data.annotation.EditorType;
import ir.asta.wise.core.data.annotation.EntityGenDirective;
import ir.asta.wise.core.data.annotation.PropertyGenDirective;
import ir.asta.wise.core.data.annotation.RelationEditorType;
import ir.asta.wise.core.data.annotation.RelationGenDirective;
import ir.asta.wise.core.data.annotation.SearchMethod;
import ir.asta.wise.core.data.annotation.SortDefault;
import ir.asta.wise.core.data.annotation.SortDirection;
import ir.asta.wise.core.data.jpa.AdaptiveDataConverter;
import ir.asta.wise.core.datamanagement.AbstractBaseEntity;
import ir.asta.wise.core.datamanagement.LogicalDeletableEntity;
import ir.asta.wise.core.datamanagement.TracableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/*PROTECTED REGION ID(ContentEntity : Imports) ENABLED START*/
/*PROTECTED REGION END*/

@Entity
@Table(name = "CONTENT")
@Audited
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", doNotUseGetters = true, callSuper = false)
@EntityGenDirective(menuParent = "root", securityParent = "root")
public class ContentEntity extends AbstractBaseEntity<java.lang.String>
		implements
			Comparable<ContentEntity>,
			TracableEntity<java.lang.String>,
			LogicalDeletableEntity,
			ir.asta.wise.core.datamanagement.AdaptiveDataEntity {
	private static final long serialVersionUID = 1L;

	/*PROTECTED REGION ID(ContentEntity Attributes) ENABLED START*/
	
	/*PROTECTED REGION END*/

	// primary key
	@Id
	@Column(name = "CONTENT_ID")
	@GenericGenerator(name = "generator", strategy = "uuid")
	@GeneratedValue(generator = "generator")
	private java.lang.String id;

	// fields
	@Basic
	@Column(name = "FILE_", nullable = true)
	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "fileName", column = @Column(name = "fileFileName")),
			@AttributeOverride(name = "contentType", column = @Column(name = "fileContentType_")),
			@AttributeOverride(name = "content", column = @Column(name = "fileContent_"))})
	@PropertyGenDirective(searchMethod = SearchMethod.EQUAL, editorType = EditorType.FILE)
	private ir.asta.wise.core.datamanagement.FileComponent file;

	@Basic
	@Column(name = "CONTENT_STATUS", nullable = true)
	@Enumerated(EnumType.STRING)
	@PropertyGenDirective(searchMethod = SearchMethod.EQUAL, editorType = EditorType.COMBO)
	private ir.asta.sample.dl.entities.ContentStatus contentStatus;

	@Transient
	public String getContentStatusStr() {
		if (contentStatus == null) {
			return null;
		}
		return contentStatus.toString();
	}

	@Basic
	@Column(name = "PURCHASE_TIME", nullable = true)
	@PropertyGenDirective(searchMethod = SearchMethod.RANGE, editorType = EditorType.DATE)
	private java.util.Date purchaseTime;

	@Basic
	@Column(name = "CODE_", nullable = true, unique = true)
	@PropertyGenDirective(searchMethod = SearchMethod.LIKE, editorType = EditorType.TEXT)
	private java.lang.String code;

	@Basic
	@Column(name = "SUBJECT_", nullable = true, unique = true)
	@PropertyGenDirective(searchMethod = SearchMethod.LIKE, editorType = EditorType.TEXT)
	private java.lang.String subject;
	
	@Basic
	@Column(name = "SORT_ORDER", nullable = true)
	@SortDefault(order = 1, dir = SortDirection.ASC)
	@PropertyGenDirective(searchMethod = SearchMethod.EQUAL, editorType = EditorType.AUTO)
	private java.lang.Long sortOrder;

	@Basic
	@Column(name = "IDENTIFIER", nullable = true)
	@PropertyGenDirective(editable = false, searchMethod = SearchMethod.LIKE, editorType = EditorType.TEXT)
	private java.lang.String identifier;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_time", nullable = true, updatable = false)
	@SortDefault(order = 0, dir = SortDirection.ASC)
	@PropertyGenDirective(editable = false, listable = false, editorType = EditorType.DATE)
	private java.util.Date creationTime;

	@Basic
	@Column(name = "created_by", nullable = true)
	@PropertyGenDirective(editable = false, viewable = false, listable = false, editorType = EditorType.TEXT)
	private java.lang.String createdBy;

	@Version
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastmodification_time", nullable = true)
	@PropertyGenDirective(editable = false, listable = false, editorType = EditorType.DATE)
	private java.util.Date lastModificationTime;

	@Basic
	@Column(name = "lastmodified_by", nullable = true)
	@PropertyGenDirective(editable = false, viewable = false, listable = false, editorType = EditorType.TEXT)
	private java.lang.String lastModifiedBy;

	@Basic
	@Column(name = "DELETED", nullable = true)
	@PropertyGenDirective(editable = false, listable = false, searchMethod = SearchMethod.LIKE, editorType = EditorType.AUTO)
	private java.lang.Boolean deleted;

	@Basic
	@Column(name = "adaptive_data_xml", length = 10000)
	@Convert(converter = AdaptiveDataConverter.class)
	private DataModel adaptiveDataModel;

	@JsonDeserialize(using = ir.asta.wise.core.remoting.rs.DataModelDeserializer.class)
	public void setAdaptiveDataModel(DataModel adaptiveDataModel) {
		this.adaptiveDataModel = adaptiveDataModel;
	}

	// many to one
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(targetEntity = ir.asta.sample.dl.entities.ContentTypeEntity.class, optional = false, cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_CONTENTTYPE", nullable = false)
	@RelationGenDirective(searchable = true, adaptiveType = true, editorType = RelationEditorType.COMBO)
	private ir.asta.sample.dl.entities.ContentTypeEntity contentType;
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(targetEntity = ir.asta.sample.dl.entities.LibraryEntity.class, optional = false, cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_LIBRARY", nullable = false)
	@RelationGenDirective(searchable = true, editorType = RelationEditorType.COMBO)
	private ir.asta.sample.dl.entities.LibraryEntity library;
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(targetEntity = ir.asta.sample.dl.entities.ConfidentialLevelEntity.class, optional = false, cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_CONFIDENTIALLEVEL", nullable = false)
	@RelationGenDirective(searchable = true, editorType = RelationEditorType.COMBO)
	private ir.asta.sample.dl.entities.ConfidentialLevelEntity confidentialLevel;

	// collections

	@NotAudited
	@OneToMany(targetEntity = ir.asta.sample.dl.entities.AttachmentEntity.class, mappedBy = "content", cascade = {})
	@RelationGenDirective(editorType = RelationEditorType.DETAILS)
	private Set<ir.asta.sample.dl.entities.AttachmentEntity> attachments;

	public int compareTo(ir.asta.sample.dl.entities.ContentEntity obj) {
		if (obj.hashCode() > hashCode())
			return 1;
		else if (obj.hashCode() < hashCode())
			return -1;
		else
			return 0;
	}
	@Override
	public String toString() {
		return toString(getSubject());
	}
	@Override
	@Transient
	protected String getToStringTemplate() {
		return "%s";
	}
	/**
	 * Don't remove this method
	 * This is related to some Lombok bugs!
	 */
	public java.lang.String getId() {
		return id;
	}

	/**
	 * Don't remove this method
	 * This is related to some Lombok bugs!
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}
	@Transient
	public java.lang.String getID() {
		return this.id;
	}
	public void setID(java.lang.String id) {
		this.id = id;
	}

	/*PROTECTED REGION ID(ContentEntity Methods) ENABLED START*/
	@Basic
	@Column(name = "EXPLANATION_", nullable = true, length = 5000)
	@PropertyGenDirective(searchMethod = SearchMethod.LIKE, editorType = EditorType.TEXT)
	private java.lang.String explanation;
	/*PROTECTED REGION END*/
}
