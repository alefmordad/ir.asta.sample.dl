package ir.asta.sample.dl.entities;

import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.gfaces.facelet.DataModel;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ir.asta.wise.core.data.annotation.EditorType;
import ir.asta.wise.core.data.annotation.EntityGenDirective;
import ir.asta.wise.core.data.annotation.PropertyGenDirective;
import ir.asta.wise.core.data.annotation.RelationEditorType;
import ir.asta.wise.core.data.annotation.RelationGenDirective;
import ir.asta.wise.core.data.annotation.SearchMethod;
import ir.asta.wise.core.data.annotation.SortDefault;
import ir.asta.wise.core.data.annotation.SortDirection;
import ir.asta.wise.core.data.jpa.AdaptiveTypeConverter;
import ir.asta.wise.core.datamanagement.AbstractBaseEntity;
import ir.asta.wise.core.datamanagement.LogicalDeletableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/*PROTECTED REGION ID(ContentTypeEntity : Imports) ENABLED START*/

/*PROTECTED REGION END*/

@Entity
@Table(name = "CONTENT_TYPE")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", doNotUseGetters = true, callSuper = false)
@EntityGenDirective(menuParent = "root", securityParent = "root")
public class ContentTypeEntity extends AbstractBaseEntity<java.lang.Long>
		implements
			Comparable<ContentTypeEntity>,
			LogicalDeletableEntity,
			ir.asta.wise.core.datamanagement.AdaptiveTypeEntity {
	private static final long serialVersionUID = 1L;

	/*PROTECTED REGION ID(ContentTypeEntity Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	// primary key
	@Id
	@Column(name = "CONTENT_TYPE_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private java.lang.Long id;

	// fields
	@Basic
	@Column(name = "CODE_", nullable = true, unique = true)
	@PropertyGenDirective(searchMethod = SearchMethod.LIKE, editorType = EditorType.TEXT)
	private java.lang.String code;

	@Basic
	@Column(name = "SUBJECT_", nullable = true, unique = true)
	@PropertyGenDirective(searchMethod = SearchMethod.LIKE, editorType = EditorType.TEXT)
	private java.lang.String subject;

	@Basic
	@Column(name = "SORT_ORDER", nullable = true)
	@SortDefault(order = 1, dir = SortDirection.ASC)
	@PropertyGenDirective(searchMethod = SearchMethod.EQUAL, editorType = EditorType.AUTO)
	private java.lang.Long sortOrder;

	@Basic
	@Column(name = "DELETED", nullable = true)
	@PropertyGenDirective(editable = false, listable = false, searchMethod = SearchMethod.LIKE, editorType = EditorType.AUTO)
	private java.lang.Boolean deleted;

	@Basic
	@Column(name = "ADAPTIVE_TYPE_XML", length = 10000)
	@Convert(converter = AdaptiveTypeConverter.class)
	private DataModel adaptiveTypeModel;

	@JsonDeserialize(using = ir.asta.wise.core.remoting.rs.DataModelDeserializer.class)
	public void setAdaptiveTypeModel(DataModel adaptiveTypeModel) {
		this.adaptiveTypeModel = adaptiveTypeModel;
	}

	// collections

	@OneToMany(targetEntity = ir.asta.sample.dl.entities.ContentEntity.class, mappedBy = "contentType", cascade = {})
	@RelationGenDirective(editorType = RelationEditorType.COMBO)
	private Set<ir.asta.sample.dl.entities.ContentEntity> contents;
	@OneToMany(targetEntity = ir.asta.sample.dl.entities.AttachmentTypeEntity.class, mappedBy = "contentType", cascade = {})
	@RelationGenDirective(editorType = RelationEditorType.COMBO)
	private Set<ir.asta.sample.dl.entities.AttachmentTypeEntity> attachmentTypes;
	@OneToMany(targetEntity = ir.asta.sample.dl.entities.LibraryEntity.class, mappedBy = "contentType", cascade = {})
	@RelationGenDirective(editorType = RelationEditorType.COMBO)
	private Set<ir.asta.sample.dl.entities.LibraryEntity> libraries;

	public int compareTo(ir.asta.sample.dl.entities.ContentTypeEntity obj) {
		if (obj.hashCode() > hashCode())
			return 1;
		else if (obj.hashCode() < hashCode())
			return -1;
		else
			return 0;
	}
	@Override
	public String toString() {
		return toString(getSubject());
	}
	@Override
	@Transient
	protected String getToStringTemplate() {
		return "%s";
	}
	/**
	 * Don't remove this method
	 * This is related to some Lombok bugs!
	 */
	public java.lang.Long getId() {
		return id;
	}

	/**
	 * Don't remove this method
	 * This is related to some Lombok bugs!
	 */
	public void setId(java.lang.Long id) {
		this.id = id;
	}
	@Transient
	public java.lang.Long getID() {
		return this.id;
	}
	public void setID(java.lang.Long id) {
		this.id = id;
	}

	/*PROTECTED REGION ID(ContentTypeEntity Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
