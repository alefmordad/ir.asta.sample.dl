package ir.asta.sample.dl.entities;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import ir.asta.wise.core.data.annotation.EditorType;
import ir.asta.wise.core.data.annotation.EntityGenDirective;
import ir.asta.wise.core.data.annotation.PropertyGenDirective;
import ir.asta.wise.core.data.annotation.RelationEditorType;
import ir.asta.wise.core.data.annotation.RelationGenDirective;
import ir.asta.wise.core.data.annotation.SearchMethod;
import ir.asta.wise.core.data.annotation.SortDefault;
import ir.asta.wise.core.data.annotation.SortDirection;
import ir.asta.wise.core.datamanagement.AbstractBaseEntity;
import ir.asta.wise.core.datamanagement.LogicalDeletableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/*PROTECTED REGION ID(AttachmentEntity : Imports) ENABLED START*/

/*PROTECTED REGION END*/

@Entity
@Table(name = "ATTACHMENT")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id", doNotUseGetters = true, callSuper = false)
@EntityGenDirective(menuParent = "root", securityParent = "root")
public class AttachmentEntity extends AbstractBaseEntity<java.lang.Long>
		implements
			Comparable<AttachmentEntity>,
			LogicalDeletableEntity {
	private static final long serialVersionUID = 1L;

	/*PROTECTED REGION ID(AttachmentEntity Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	// primary key
	@Id
	@Column(name = "ATTACHMENT_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private java.lang.Long id;

	// fields
	@Basic
	@Column(name = "FILE_", nullable = true)
	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "fileName", column = @Column(name = "fileFileName")),
			@AttributeOverride(name = "contentType", column = @Column(name = "fileContentType_")),
			@AttributeOverride(name = "content", column = @Column(name = "fileContent_"))})
	@PropertyGenDirective(searchMethod = SearchMethod.EQUAL, editorType = EditorType.FILE)
	private ir.asta.wise.core.datamanagement.FileComponent file;

	@Basic
	@Column(name = "CODE_", nullable = true, unique = true)
	@PropertyGenDirective(searchMethod = SearchMethod.LIKE, editorType = EditorType.TEXT)
	private java.lang.String code;

	@Basic
	@Column(name = "SUBJECT_", nullable = true, unique = true)
	@PropertyGenDirective(searchMethod = SearchMethod.LIKE, editorType = EditorType.TEXT)
	private java.lang.String subject;

	@Basic
	@Column(name = "SORT_ORDER", nullable = true)
	@SortDefault(order = 1, dir = SortDirection.ASC)
	@PropertyGenDirective(searchMethod = SearchMethod.EQUAL, editorType = EditorType.AUTO)
	private java.lang.Long sortOrder;

	@Basic
	@Column(name = "DELETED", nullable = true)
	@PropertyGenDirective(editable = false, listable = false, searchMethod = SearchMethod.LIKE, editorType = EditorType.AUTO)
	private java.lang.Boolean deleted;

	// many to one
	@ManyToOne(targetEntity = ir.asta.sample.dl.entities.ContentEntity.class, optional = false, cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_CONTENT", nullable = false)
	@RelationGenDirective(editorType = RelationEditorType.MASTER)
	private ir.asta.sample.dl.entities.ContentEntity content;
	@ManyToOne(targetEntity = ir.asta.sample.dl.entities.AttachmentTypeEntity.class, optional = false, cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ATTACHMENTTYPE", nullable = false)
	@RelationGenDirective(searchable = true, editorType = RelationEditorType.COMBO)
	private ir.asta.sample.dl.entities.AttachmentTypeEntity attachmentType;

	public int compareTo(ir.asta.sample.dl.entities.AttachmentEntity obj) {
		if (obj.hashCode() > hashCode())
			return 1;
		else if (obj.hashCode() < hashCode())
			return -1;
		else
			return 0;
	}
	@Override
	public String toString() {
		return toString(getSubject());
	}
	@Override
	@Transient
	protected String getToStringTemplate() {
		return "%s";
	}
	/**
	 * Don't remove this method
	 * This is related to some Lombok bugs!
	 */
	public java.lang.Long getId() {
		return id;
	}

	/**
	 * Don't remove this method
	 * This is related to some Lombok bugs!
	 */
	public void setId(java.lang.Long id) {
		this.id = id;
	}
	@Transient
	public java.lang.Long getID() {
		return this.id;
	}
	public void setID(java.lang.Long id) {
		this.id = id;
	}

	/*PROTECTED REGION ID(AttachmentEntity Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
