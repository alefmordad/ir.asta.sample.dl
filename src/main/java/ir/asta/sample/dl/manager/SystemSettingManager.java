package ir.asta.sample.dl.manager;

import javax.inject.Inject;
import javax.inject.Named;

import ir.asta.sample.dl.dao.SystemSettingDao;

/*PROTECTED REGION ID(SystemSettingManager Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.SystemSettingEntity;
import ir.asta.wise.core.exceptions.LocalizedException;

@Named("systemSettingManager")
public class SystemSettingManager
		extends ir.asta.wise.core.crud.AbstractCrudManager<SystemSettingEntity, java.lang.Long> {

	/* PROTECTED REGION ID(SystemSettingManager Attributes) ENABLED START */

	/* PROTECTED REGION END */

	@Inject
	public SystemSettingManager(SystemSettingDao dao) {
		super.setDao(dao);
	}

	@SuppressWarnings("unused")
	private SystemSettingDao getMyDao() {
		return (SystemSettingDao) super.getDao();
	}

	/* PROTECTED REGION ID(SystemSettingManager Methods) ENABLED START */
	
	@Override
	protected void beforeMerge(SystemSettingEntity entity, boolean creating) {
		if (creating) {
			if (loadAll().size() != 0)
				throw new LocalizedException("can't have more than one entity of SystemSetting");
		}
	}
	
	/* PROTECTED REGION END */
}
