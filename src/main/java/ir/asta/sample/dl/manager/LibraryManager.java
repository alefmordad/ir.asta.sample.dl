package ir.asta.sample.dl.manager;

import javax.inject.Inject;
import javax.inject.Named;

/*PROTECTED REGION ID(LibraryManager Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.LibraryEntity;
import ir.asta.sample.dl.dao.LibraryDao;

@Named("libraryManager")
public class LibraryManager
		extends
			ir.asta.wise.core.crud.AbstractCrudManager<LibraryEntity, java.lang.Long> {

	/*PROTECTED REGION ID(LibraryManager Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	@Inject
	public LibraryManager(LibraryDao dao) {
		super.setDao(dao);
	}

	@SuppressWarnings("unused")
	private LibraryDao getMyDao() {
		return (LibraryDao) super.getDao();
	}

	/*PROTECTED REGION ID(LibraryManager Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
