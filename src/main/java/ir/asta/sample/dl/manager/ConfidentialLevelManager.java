package ir.asta.sample.dl.manager;

import javax.inject.Inject;
import javax.inject.Named;

/*PROTECTED REGION ID(ConfidentialLevelManager Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.ConfidentialLevelEntity;
import ir.asta.sample.dl.dao.ConfidentialLevelDao;

@Named("confidentialLevelManager")
public class ConfidentialLevelManager
		extends
			ir.asta.wise.core.crud.AbstractCrudManager<ConfidentialLevelEntity, java.lang.Long> {

	/*PROTECTED REGION ID(ConfidentialLevelManager Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	@Inject
	public ConfidentialLevelManager(ConfidentialLevelDao dao) {
		super.setDao(dao);
	}

	@SuppressWarnings("unused")
	private ConfidentialLevelDao getMyDao() {
		return (ConfidentialLevelDao) super.getDao();
	}

	/*PROTECTED REGION ID(ConfidentialLevelManager Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
