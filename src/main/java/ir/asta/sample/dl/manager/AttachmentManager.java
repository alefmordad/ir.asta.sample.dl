package ir.asta.sample.dl.manager;

import javax.inject.Inject;
import javax.inject.Named;

/*PROTECTED REGION ID(AttachmentManager Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.AttachmentEntity;
import ir.asta.sample.dl.dao.AttachmentDao;

@Named("attachmentManager")
public class AttachmentManager
		extends
			ir.asta.wise.core.crud.AbstractCrudManager<AttachmentEntity, java.lang.Long> {

	/*PROTECTED REGION ID(AttachmentManager Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	@Inject
	public AttachmentManager(AttachmentDao dao) {
		super.setDao(dao);
	}

	@SuppressWarnings("unused")
	private AttachmentDao getMyDao() {
		return (AttachmentDao) super.getDao();
	}

	/*PROTECTED REGION ID(AttachmentManager Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
