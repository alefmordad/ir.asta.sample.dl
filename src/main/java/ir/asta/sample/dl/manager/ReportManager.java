package ir.asta.sample.dl.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.Tuple;

import ir.asta.sample.dl.dao.ContentDao;
import ir.asta.sample.dl.dao.meta.QContent;
import ir.asta.sample.dl.entities.ContentEntity;
import ir.asta.sample.dl.model.Report;
import ir.asta.sample.dl.util.converters.SearchFilterToSearchParamCoverter;
import ir.asta.sample.dl.util.converters.TupleToReportConverter;
import ir.asta.wise.core.data.querydsl.QueryDslDynamicQueryBuilder;
import ir.asta.wise.core.data.querydsl.SmartQuery;
import ir.asta.wise.core.datamanagement.SearchParam;

@Named("reportManager")
public class ReportManager {
	
	@Inject
	private ContentDao contentDao;

	@Transactional(readOnly = true)
	public List<Report> getContentByContentTypeReport(SearchParam<Map<String, Object>> searchParam) {
		QContent content = new QContent("content");
		SmartQuery<Tuple> query = contentDao.createQuery().select(content.id.count(), content.contentType.subject).from(content).groupBy(content.contentType.subject);
		QueryDslDynamicQueryBuilder builder = new QueryDslDynamicQueryBuilder(query, ContentEntity.class);
		builder.build(new SearchFilterToSearchParamCoverter().convert(searchParam));
		List<Tuple> result = query.fetch();
		List<Report> reports = new ArrayList<>();
		TupleToReportConverter convertor = new TupleToReportConverter();
		for (Tuple tuple : result)
			reports.add(convertor.convert(tuple));
		return reports;
	}
	
}
