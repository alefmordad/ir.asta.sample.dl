package ir.asta.sample.dl.manager;

import javax.inject.Inject;
import javax.inject.Named;

/*PROTECTED REGION ID(AttachmentTypeManager Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.AttachmentTypeEntity;
import ir.asta.sample.dl.dao.AttachmentTypeDao;

@Named("attachmentTypeManager")
public class AttachmentTypeManager
		extends
			ir.asta.wise.core.crud.AbstractCrudManager<AttachmentTypeEntity, java.lang.Long> {

	/*PROTECTED REGION ID(AttachmentTypeManager Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	@Inject
	public AttachmentTypeManager(AttachmentTypeDao dao) {
		super.setDao(dao);
	}

	@SuppressWarnings("unused")
	private AttachmentTypeDao getMyDao() {
		return (AttachmentTypeDao) super.getDao();
	}

	/*PROTECTED REGION ID(AttachmentTypeManager Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
