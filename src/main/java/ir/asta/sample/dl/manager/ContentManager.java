package ir.asta.sample.dl.manager;

import javax.inject.Inject;
import javax.inject.Named;

/*PROTECTED REGION ID(ContentManager Imports) ENABLED START*/
import java.util.Date;
import java.util.Map;
import org.springframework.transaction.annotation.Transactional;

import ir.asta.sample.dl.dao.ContentDao;
import ir.asta.sample.dl.entities.ContentEntity;
import ir.asta.sample.dl.entities.ContentStatus;
import ir.asta.sample.dl.util.DlConstants;
import ir.asta.wise.core.datamanagement.DataPage;
import ir.asta.wise.core.datamanagement.SearchParam;
import ir.asta.wise.core.datamanagement.VersionEntity;
import ir.asta.wise.core.security.AuthorizationException;
import ir.asta.wise.core.security.SecurityContextUtils;
import ir.asta.wise.core.util.db.SequenceGenerator;
import ir.asta.wise.portal.security.model.UserDTO;
import ir.asta.wise.portal.security.service.query.user.UserQueryFactory;
/*PROTECTED REGION END*/

@Named("contentManager")
public class ContentManager extends ir.asta.wise.core.crud.AbstractCrudManager<ContentEntity, java.lang.String> {

	/* PROTECTED REGION ID(ContentManager Attributes) ENABLED START */
	@Inject
	private UserQueryFactory userQueryFactory;
	@Inject
	private SequenceGenerator sequenceGenerator;
	/* PROTECTED REGION END */

	@Inject
	public ContentManager(ContentDao dao) {
		super.setDao(dao);
	}

	@SuppressWarnings("unused")
	private ContentDao getMyDao() {
		return (ContentDao) super.getDao();
	}

	/* PROTECTED REGION ID(ContentManager Methods) ENABLED START */
	@Override
	@Transactional(readOnly = true)
	public DataPage<VersionEntity<ContentEntity>> searchVersions(String id,
			SearchParam<Map<String, Object>> searchParam) {
		DataPage<VersionEntity<ContentEntity>> searchVersions = super.searchVersions(id, searchParam);
		addInfo(searchVersions);
		return searchVersions;
	}

	private void addInfo(DataPage<VersionEntity<ContentEntity>> searchVersions) {
		for (VersionEntity<ContentEntity> versionEntity : searchVersions.getItems()) {
			String committerId = versionEntity.getRevInfo().getCommitterId();
			UserDTO committer = userQueryFactory.create().addUserIdentifier(committerId).singleResult();
			String committerName = committer.getFirstName() + " " + committer.getLastName();
			versionEntity.getRevInfo().setCommitterName(committerName);
		}
	}

	@Override
	public ContentEntity createEntity() {
		ContentEntity contentEntity = super.createEntity();
		contentEntity.setPurchaseTime(new Date());
		return contentEntity;
	}

	@Override
	public void beforeMerge(ContentEntity entity, boolean creating) {
		if (creating) {
			entity.setIdentifier(sequenceGenerator.generate());
		} else {
			ContentStatus oldContentStatus = load(entity.getId()).getContentStatus();
			ContentStatus newContentStatus = entity.getContentStatus();
			if (oldContentStatus != newContentStatus) {
				if (oldContentStatus != ContentStatus.WAITING) {
					throw new AuthorizationException();
				}
				if (!SecurityContextUtils.isCurrentUserAuthorizedForPermission(DlConstants.PERMISSION_CONTENT_ACCEPT)) {
					throw new AuthorizationException();
				}
			}
		}
	}

	@Override
	@Transactional(readOnly = true)
	public ContentEntity load(String id) {
		ContentEntity contentEntity = super.load(id);
		contentEntity.getContentType().getId();
		return contentEntity;
	}
	/* PROTECTED REGION END */
}
