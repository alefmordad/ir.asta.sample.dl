package ir.asta.sample.dl.dao;

import javax.inject.Named;
import org.springframework.stereotype.Repository;

/*PROTECTED REGION ID(AttachmentDAO Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.AttachmentEntity;

@Repository
@Named("attachmentDao")
public class AttachmentDao
		extends
			ir.asta.wise.core.data.jpa.AbstractJpaDao<AttachmentEntity, java.lang.Long> {

	/*PROTECTED REGION ID(AttachmentDAO Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	/*PROTECTED REGION ID(AttachmentDAO Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
