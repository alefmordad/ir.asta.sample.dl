package ir.asta.sample.dl.dao;

import javax.inject.Named;
import org.springframework.stereotype.Repository;

/*PROTECTED REGION ID(AttachmentTypeDAO Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.AttachmentTypeEntity;

@Repository
@Named("attachmentTypeDao")
public class AttachmentTypeDao
		extends
			ir.asta.wise.core.data.jpa.AbstractJpaDao<AttachmentTypeEntity, java.lang.Long> {

	/*PROTECTED REGION ID(AttachmentTypeDAO Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	/*PROTECTED REGION ID(AttachmentTypeDAO Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
