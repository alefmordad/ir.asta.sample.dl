package ir.asta.sample.dl.dao.meta;
import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import ir.asta.sample.dl.entities.*;

@Generated("com.querydsl.codegen.EntitySerializer")
public class QLibrary extends PathBuilder<LibraryEntity> {
	private static final long serialVersionUID = -785829409L;

	private static final PathInits INITS = PathInits.DIRECT2;

	public final NumberPath<Long> id = createNumber("id", Long.class);
	public final StringPath code = createString("code");
	public final StringPath subejct = createString("subject");
	public final NumberPath<Long> sortOrder = createNumber("sortOrder",
			Long.class);
	public final BooleanPath deleted = createBoolean("deleted");

	public SetPath<ir.asta.sample.dl.entities.ContentEntity, ir.asta.sample.dl.dao.meta.QContent> contents = this
			.<ir.asta.sample.dl.entities.ContentEntity, ir.asta.sample.dl.dao.meta.QContent> createSet(
					"contents", ir.asta.sample.dl.entities.ContentEntity.class,
					ir.asta.sample.dl.dao.meta.QContent.class,
					PathInits.DIRECT2);

	public final ir.asta.sample.dl.dao.meta.QContentType contentType;

	public QLibrary(String variable) {
		this(LibraryEntity.class, forVariable(variable), INITS);
	}

	public QLibrary(Path<? extends LibraryEntity> path) {
		this(path.getType(), path.getMetadata(), PathInits.getFor(path
				.getMetadata(), INITS));
	}

	public QLibrary(PathMetadata metadata) {
		this(metadata, PathInits.getFor(metadata, INITS));
	}

	public QLibrary(PathMetadata metadata, PathInits inits) {
		this(LibraryEntity.class, metadata, inits);
	}

	public QLibrary(Class<? extends LibraryEntity> type, PathMetadata metadata,
			PathInits inits) {
		super(type, metadata);
		this.contentType = inits.isInitialized("contentType")
				? new ir.asta.sample.dl.dao.meta.QContentType(
						forProperty("contentType"))
				: null;

	}

}
