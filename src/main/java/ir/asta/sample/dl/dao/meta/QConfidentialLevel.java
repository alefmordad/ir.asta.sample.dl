package ir.asta.sample.dl.dao.meta;
import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import ir.asta.sample.dl.entities.*;

@Generated("com.querydsl.codegen.EntitySerializer")
public class QConfidentialLevel extends PathBuilder<ConfidentialLevelEntity> {
	private static final long serialVersionUID = -785829409L;

	private static final PathInits INITS = PathInits.DIRECT2;

	public final NumberPath<Long> id = createNumber("id", Long.class);
	public final StringPath subject = createString("subject");
	public final NumberPath<Long> levelIndex = createNumber("levelIndex",
			Long.class);

	public QConfidentialLevel(String variable) {
		this(ConfidentialLevelEntity.class, forVariable(variable), INITS);
	}

	public QConfidentialLevel(Path<? extends ConfidentialLevelEntity> path) {
		this(path.getType(), path.getMetadata(), PathInits.getFor(path
				.getMetadata(), INITS));
	}

	public QConfidentialLevel(PathMetadata metadata) {
		this(metadata, PathInits.getFor(metadata, INITS));
	}

	public QConfidentialLevel(PathMetadata metadata, PathInits inits) {
		this(ConfidentialLevelEntity.class, metadata, inits);
	}

	public QConfidentialLevel(Class<? extends ConfidentialLevelEntity> type,
			PathMetadata metadata, PathInits inits) {
		super(type, metadata);

	}

}
