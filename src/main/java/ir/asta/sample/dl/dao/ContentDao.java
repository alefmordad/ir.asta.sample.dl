package ir.asta.sample.dl.dao;

import javax.inject.Named;
import org.springframework.stereotype.Repository;

/*PROTECTED REGION ID(ContentDAO Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.ContentEntity;

@Repository
@Named("contentDao")
public class ContentDao
		extends
			ir.asta.wise.core.data.jpa.AbstractJpaDao<ContentEntity, java.lang.String> {

	/*PROTECTED REGION ID(ContentDAO Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	/*PROTECTED REGION ID(ContentDAO Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
