package ir.asta.sample.dl.dao.meta;
import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import ir.asta.sample.dl.entities.*;

@Generated("com.querydsl.codegen.EntitySerializer")
public class QAttachment extends PathBuilder<AttachmentEntity> {
	private static final long serialVersionUID = -785829409L;

	private static final PathInits INITS = PathInits.DIRECT2;

	public final NumberPath<Long> id = createNumber("id", Long.class);
	public final StringPath code = createString("code");
	public final StringPath subject = createString("subject");
	public final NumberPath<Long> sortOrder = createNumber("sortOrder",
			Long.class);
	public final BooleanPath deleted = createBoolean("deleted");

	public final ir.asta.sample.dl.dao.meta.QContent content;
	public final ir.asta.sample.dl.dao.meta.QAttachmentType attachmentType;

	public QAttachment(String variable) {
		this(AttachmentEntity.class, forVariable(variable), INITS);
	}

	public QAttachment(Path<? extends AttachmentEntity> path) {
		this(path.getType(), path.getMetadata(), PathInits.getFor(path
				.getMetadata(), INITS));
	}

	public QAttachment(PathMetadata metadata) {
		this(metadata, PathInits.getFor(metadata, INITS));
	}

	public QAttachment(PathMetadata metadata, PathInits inits) {
		this(AttachmentEntity.class, metadata, inits);
	}

	public QAttachment(Class<? extends AttachmentEntity> type,
			PathMetadata metadata, PathInits inits) {
		super(type, metadata);
		this.content = inits.isInitialized("content")
				? new ir.asta.sample.dl.dao.meta.QContent(
						forProperty("content"))
				: null;
		this.attachmentType = inits.isInitialized("attachmentType")
				? new ir.asta.sample.dl.dao.meta.QAttachmentType(
						forProperty("attachmentType"))
				: null;

	}

}
