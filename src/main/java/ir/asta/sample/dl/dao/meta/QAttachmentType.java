package ir.asta.sample.dl.dao.meta;
import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import ir.asta.sample.dl.entities.*;

@Generated("com.querydsl.codegen.EntitySerializer")
public class QAttachmentType extends PathBuilder<AttachmentTypeEntity> {
	private static final long serialVersionUID = -785829409L;

	private static final PathInits INITS = PathInits.DIRECT2;

	public final NumberPath<Long> id = createNumber("id", Long.class);
	public final StringPath code = createString("code");
	public final StringPath subject = createString("subject");
	public final NumberPath<Long> sortOrder = createNumber("sortOrder",
			Long.class);
	public final BooleanPath deleted = createBoolean("deleted");

	public SetPath<ir.asta.sample.dl.entities.AttachmentEntity, ir.asta.sample.dl.dao.meta.QAttachment> attachments = this
			.<ir.asta.sample.dl.entities.AttachmentEntity, ir.asta.sample.dl.dao.meta.QAttachment> createSet(
					"attachments",
					ir.asta.sample.dl.entities.AttachmentEntity.class,
					ir.asta.sample.dl.dao.meta.QAttachment.class,
					PathInits.DIRECT2);

	public final ir.asta.sample.dl.dao.meta.QContentType contentType;

	public QAttachmentType(String variable) {
		this(AttachmentTypeEntity.class, forVariable(variable), INITS);
	}

	public QAttachmentType(Path<? extends AttachmentTypeEntity> path) {
		this(path.getType(), path.getMetadata(), PathInits.getFor(path
				.getMetadata(), INITS));
	}

	public QAttachmentType(PathMetadata metadata) {
		this(metadata, PathInits.getFor(metadata, INITS));
	}

	public QAttachmentType(PathMetadata metadata, PathInits inits) {
		this(AttachmentTypeEntity.class, metadata, inits);
	}

	public QAttachmentType(Class<? extends AttachmentTypeEntity> type,
			PathMetadata metadata, PathInits inits) {
		super(type, metadata);
		this.contentType = inits.isInitialized("contentType")
				? new ir.asta.sample.dl.dao.meta.QContentType(
						forProperty("contentType"))
				: null;

	}

}
