package ir.asta.sample.dl.dao;

import javax.inject.Named;
import org.springframework.stereotype.Repository;

/*PROTECTED REGION ID(SystemSettingDAO Imports) ENABLED START*/

/*PROTECTED REGION END*/

import ir.asta.sample.dl.entities.SystemSettingEntity;

@Repository
@Named("systemSettingDao")
public class SystemSettingDao
		extends
			ir.asta.wise.core.data.jpa.AbstractJpaDao<SystemSettingEntity, java.lang.Long> {

	/*PROTECTED REGION ID(SystemSettingDAO Attributes) ENABLED START*/

	/*PROTECTED REGION END*/

	/*PROTECTED REGION ID(SystemSettingDAO Methods) ENABLED START*/

	/*PROTECTED REGION END*/
}
