package ir.asta.sample.dl.dao.meta;
import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import ir.asta.sample.dl.entities.*;

@Generated("com.querydsl.codegen.EntitySerializer")
public class QSystemSetting extends PathBuilder<SystemSettingEntity> {
	private static final long serialVersionUID = -785829409L;

	private static final PathInits INITS = PathInits.DIRECT2;

	public final NumberPath<Long> id = createNumber("id", Long.class);

	public QSystemSetting(String variable) {
		this(SystemSettingEntity.class, forVariable(variable), INITS);
	}

	public QSystemSetting(Path<? extends SystemSettingEntity> path) {
		this(path.getType(), path.getMetadata(), PathInits.getFor(path
				.getMetadata(), INITS));
	}

	public QSystemSetting(PathMetadata metadata) {
		this(metadata, PathInits.getFor(metadata, INITS));
	}

	public QSystemSetting(PathMetadata metadata, PathInits inits) {
		this(SystemSettingEntity.class, metadata, inits);
	}

	public QSystemSetting(Class<? extends SystemSettingEntity> type,
			PathMetadata metadata, PathInits inits) {
		super(type, metadata);

	}

}
