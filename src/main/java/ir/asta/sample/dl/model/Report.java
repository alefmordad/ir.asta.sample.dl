package ir.asta.sample.dl.model;

public class Report {

	private String contentTypeSubject;
	private Long count;

	public String getContentTypeSubject() {
		return contentTypeSubject;
	}

	public void setContentTypeSubject(String contentTypeSubject) {
		this.contentTypeSubject = contentTypeSubject;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Report(String contentTypeSubject, Long count) {
		super();
		this.contentTypeSubject = contentTypeSubject;
		this.count = count;
	}

	public Report() {
		super();
	}

}