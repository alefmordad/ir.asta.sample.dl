package ir.asta.sample.dl.model;

import ir.asta.sample.dl.entities.ContentEntity;
import ir.asta.sample.dl.entities.ContentStatus;

public class ContentStatusUpdateModel {

	private String id;
	private ContentStatus contentStatus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ContentStatus getContentStatus() {
		return contentStatus;
	}

	public void setContentStatus(ContentStatus contentStatus) {
		this.contentStatus = contentStatus;
	}
	
	public ContentEntity getEntity() {
		ContentEntity contentEntity = new ContentEntity();
		contentEntity.setId(id);
		contentEntity.setContentStatus(contentStatus);
		return contentEntity;
	}

}
