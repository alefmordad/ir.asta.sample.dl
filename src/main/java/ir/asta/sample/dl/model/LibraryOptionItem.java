package ir.asta.sample.dl.model;

import java.util.ArrayList;

import ir.asta.sample.dl.entities.ContentTypeEntity;
import ir.asta.sample.dl.entities.LibraryEntity;
import ir.asta.wise.core.datamanagement.DataPage;

public class LibraryOptionItem {

	private Long pk;
	private String toStr;
	private String code;
	private ContentTypeEntity contentType;

	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getToStr() {
		return toStr;
	}

	public void setToStr(String toStr) {
		this.toStr = toStr;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ContentTypeEntity getContentType() {
		return contentType;
	}

	public void setContentType(ContentTypeEntity contentType) {
		this.contentType = contentType;
	}
	
	public LibraryOptionItem() {}

	public LibraryOptionItem(LibraryEntity libraryEntity) {
		setPk(libraryEntity.getId());
		setCode(libraryEntity.getCode());
		setToStr(libraryEntity.getToString());
		setContentType(libraryEntity.getContentType());
	}

	public static DataPage<LibraryOptionItem> convertFromDataPage(DataPage<LibraryEntity> source) {
		DataPage<LibraryOptionItem> res = new DataPage<>(source);
		res.setItems(new ArrayList<LibraryOptionItem>());
		if (source.getItems() != null) {
			for (LibraryEntity e : source.getItems()) {
				res.getItems().add(new LibraryOptionItem(e));
			}
		}
		return res;
	}

}
