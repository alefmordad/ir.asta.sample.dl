package ir.asta.sample.dl.util.converters;

import com.querydsl.core.Tuple;

import ir.asta.sample.dl.model.Report;

public class TupleToReportConverter implements Converter<Tuple, Report>{

	@Override
	public Report convert(Tuple tuple) {
		Report report = new Report();
		report.setCount(tuple.get(0, Long.class));
		report.setContentTypeSubject(tuple.get(1, String.class));
		return report;
	}

}
