package ir.asta.sample.dl.util.converters;

public class ResourceNameToPathConverter implements Converter<String, String> {

	@Override
	public String convert(String name) {
		return getClass().getClassLoader().getResource(name).getPath().substring(1);
	}

}
