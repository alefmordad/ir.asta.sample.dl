package ir.asta.sample.dl.util;

public class DlConstants {
	public static final String PROP_CONFIDENTIAL_LEVEL = "confidentialLevel";
	public static final String PROP_LEVEL_INDEX = "levelIndex";
	public static final String PROP_CONTENT_STATUS = "contentStatus";
	public static final String PROP_CONTENT_TYPE = "contentType";
	public static final String PROP_ID = "id";
	public static final String PERMISSION_CONTENT_ACCEPT_CAMELCASE = "contentAccept";
	public static final String PERMISSION_CONTENT_ACCEPT = "content.accept";
	public static final String MSG_CONTENT_UPDATE_CONTENTSTATUS_SUCCESS = "content_update_contentStatus_success";
	public static final String MSG_CONTENT_UPDATE_CONTENTSTATUS_FAILURE = "content_update_contentStatus_failure";

}
