package ir.asta.sample.dl.util.converters;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.BeanUtils;

import ir.asta.wise.core.datamanagement.SearchParam;

public class HttpServletRequestToSearchParamConverter implements Converter<HttpServletRequest, SearchParam<Map<String, Object>>> {

	@Override
	public SearchParam<Map<String, Object>> convert(HttpServletRequest request) {
		SearchParam<Map<String, Object>> searchParam = new SearchParam<Map<String, Object>>();
		searchParam.setFilter(new HashMap<>());
		for (Enumeration<String> e = request.getParameterNames(); e.hasMoreElements();) {
			String key = e.nextElement();
			String value = request.getParameter(key);
			PropertyDescriptor propertyDescriptor = BeanUtils.getPropertyDescriptor(SearchParam.class, key);
			if (propertyDescriptor != null
					&& BeanUtils.isSimpleValueType(propertyDescriptor.getPropertyType())) {
				try {
					PropertyUtils.setSimpleProperty(searchParam, key,
							ConvertUtils.convert(value, propertyDescriptor.getPropertyType()));
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e1) {
				}
			} else {
				searchParam.getFilter().put(key, value);
			}
		}
		return searchParam;
	}

}
