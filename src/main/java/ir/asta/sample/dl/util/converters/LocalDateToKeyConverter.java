package ir.asta.sample.dl.util.converters;

import java.time.LocalDate;

public class LocalDateToKeyConverter implements Converter<LocalDate, String> {

	@Override
	public String convert(LocalDate date) {
		return date.getYear() + "/" + date.getMonthValue() + "/" + date.getDayOfMonth();
	}

}
