package ir.asta.sample.dl.util.converters;

public interface Converter<I, O> {
	
	O convert(I input);

}
