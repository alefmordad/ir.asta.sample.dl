package ir.asta.sample.dl.util.converters;

import java.util.Map;

import ir.asta.wise.core.datamanagement.SearchFilter;
import ir.asta.wise.core.datamanagement.SearchParam;
import ir.asta.wise.core.datamanagement.SearchFilter.BasicFilterExpression;

public class SearchFilterToSearchParamCoverter implements Converter<SearchParam<Map<String, Object>>, SearchFilter<Object>> {

	@Override
	public SearchFilter<Object> convert(SearchParam<Map<String, Object>> searchParam) {
		SearchFilter<Object> searchFilter = new SearchFilter<>();
		for (String key : searchParam.getFilter().keySet()) {
			searchFilter.add(new BasicFilterExpression<Object>(key.split(":")[0] + ":", key.split(":")[1], searchParam.getFilter().get(key)));
		}
		return searchFilter;
	}

}
